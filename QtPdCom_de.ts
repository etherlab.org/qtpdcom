<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">Hauptfenster</translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="vanished">Anwendung</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Sprache</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Trennen</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Verbinden</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Schließen</translation>
    </message>
    <message>
        <source>Deutsch</source>
        <translation type="vanished">Deutsch</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">Englisch</translation>
    </message>
    <message>
        <source>Nederlands</source>
        <translation type="vanished">Niederländisch</translation>
    </message>
    <message>
        <source>Connection error</source>
        <translation type="vanished">Verbindungsfehler</translation>
    </message>
    <message>
        <source>Failed to connect to data source.</source>
        <translation type="vanished">Konnte nicht mit Datenquelle verbinden.</translation>
    </message>
</context>
<context>
    <name>QtPdCom::BroadcastModel</name>
    <message>
        <location filename="src/BroadcastModel.cpp" line="119"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="src/BroadcastModel.cpp" line="121"/>
        <source>Message</source>
        <translation>Nachricht</translation>
    </message>
    <message>
        <location filename="src/BroadcastModel.cpp" line="123"/>
        <source>User</source>
        <translation>Benutzer</translation>
    </message>
</context>
<context>
    <name>QtPdCom::ClientStatisticsModel</name>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="110"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="112"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="114"/>
        <source>Received bytes</source>
        <translation>Empfangene Bytes</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="116"/>
        <source>Sent bytes</source>
        <translation>Gesendete Bytes</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="118"/>
        <source>Connected time</source>
        <translation>Verbunden seit</translation>
    </message>
</context>
<context>
    <name>QtPdCom::LoginManager</name>
    <message>
        <location filename="src/LoginManager.cpp" line="148"/>
        <source>Login successful.</source>
        <translation>Anmeldung erfolgreich.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="150"/>
        <source>Login failed: Please check the given username and password.</source>
        <translation>Anmeldung fehlgeschlagen: Bitte überprüfen Sie den angegebenen Nutzernamen und das Kennwort.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="153"/>
        <source>Login was aborted.</source>
        <translation>Die Anmeldung wurde abgebrochen.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="155"/>
        <source>Login failed: No authentication mechanism found.</source>
        <translation>Anmeldung fehlgeschlagen: Kein Authentifizierungsmechanismus gefunden.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="157"/>
        <source>Unknown login error</source>
        <translation>Unbekannter Fehler bei der Anmeldung</translation>
    </message>
</context>
<context>
    <name>QtPdCom::MessageModel</name>
    <message>
        <location filename="src/MessageModel.cpp" line="69"/>
        <source>Failed to open %1.</source>
        <translation>Fehler beim Öffnen von %1.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="79"/>
        <location filename="src/MessageModel.cpp" line="92"/>
        <source>Failed to parse %1, line %2, column %3: %4</source>
        <translation>Fehler beim Parsen von %1, Zeile %2, Spalte %3: %4</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="107"/>
        <source>Failed to process %1: No plain message file (%2)!</source>
        <translation>Konnte %1 nicht verarbeiten, da es keine flache Meldungsdatei ist (%2)!</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="225"/>
        <source>Failed to connect to message manager.</source>
        <translation>Konnte nicht mit Meldungsverwaltung verbinden.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="431"/>
        <source>Reset</source>
        <translation>Quittiert</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="251"/>
        <source>Failed to subscribe to %1: %2</source>
        <translation>Fehler beim Abonnieren von %1: %2</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="425"/>
        <source>Message</source>
        <translation>Meldung</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="428"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
</context>
</TS>
