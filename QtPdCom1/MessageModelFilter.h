/*****************************************************************************
 *
 * Copyright (C) 2009 - 2024  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_MESSAGEMODELFILTER_H
#define QTPDCOM_MESSAGEMODELFILTER_H

#include "Export.h"
#include <QScopedPointer>
#include <QSortFilterProxyModel>

namespace QtPdCom {

class MessageModelFilterPrivate;

/** Proxy model to filter Messages based on their reset time.
 *
 * This Proxy model allows to filter out acknowledged messages.
 * Messages with a non-empty reset timestamp are acknowledged.
 *
 * This class is also available in QML.
 *
 * Suppy a MessageModel instance as \c sourceModel property.
 */
class QTPDCOM_PUBLIC MessageModelFilter: public QSortFilterProxyModel
{
        Q_OBJECT
        Q_PROPERTY(bool showOnlyActiveMessages READ getShowOnlyActiveMessages
                           WRITE setShowOnlyActiveMessages)
    public:
        explicit MessageModelFilter(QObject *parent = nullptr);
        ~MessageModelFilter();
        bool getShowOnlyActiveMessages() const;
        void setShowOnlyActiveMessages(bool value);

    protected:
        bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent)
                const override;

    private:
        Q_DECLARE_PRIVATE(MessageModelFilter);
        QScopedPointer<MessageModelFilterPrivate> const d_ptr;
};

}  // namespace QtPdCom

#endif  // QTPDCOM_MESSAGEMODELFILTER_H
