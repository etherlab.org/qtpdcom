/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef QTPDCOM_FUTUREWATCHERS_DETAILS_H
#define QTPDCOM_FUTUREWATCHERS_DETAILS_H

#include <QFutureWatcher>

#include <type_traits>

namespace QtPdCom { namespace details {

template <class T> using void_t = void;

template <class Obj, class... Arg> struct takes_obj_as_first_parameter_i
{
    template <class Func> static bool value(...)
    {
        throw "argument mismatch. Make sure that your callback takes "
              "(optionally) a reference to Object as first argument "
              "and the result of the Future (if it is non-void) as value or "
              "const reference as the other argument";
    }

    template <
            class Func,
            typename = void_t<
                    decltype(std::declval<Func>()(std::declval<Arg>()...))>>
    static constexpr bool value(char)
    {
        return false;
    }

    template <
            class Func,
            typename = void_t<decltype(std::declval<Func>()(
                    std::declval<Obj &>(),
                    std::declval<Arg>()...))>>
    static constexpr bool value(int)
    {
        return true;
    }
};

template <class Func, class Obj, class... Args>
constexpr bool takes_obj_as_first_parameter()
{
    return takes_obj_as_first_parameter_i<Obj, Args...>::template value<Func>(
            0);
}

template <bool with_obj, bool with_result> struct call_lambda
{
    template <class Result, class Obj, class Func> static void
    call(Func &&func, Obj &obj, QFutureWatcher<Result> const *watcher)
    {
        func(obj, watcher->result());
    }
};

template <> struct call_lambda<false, true>
{
    template <class Result, class Obj, class Func> static void
    call(Func &&func, Obj &, QFutureWatcher<Result> const *watcher)
    {
        func(watcher->result());
    }
};
template <> struct call_lambda<true, false>
{
    template <class Result, class Obj, class Func>
    static void call(Func &&func, Obj &obj, QFutureWatcher<Result> const *)
    {
        func(obj);
    }
};

template <> struct call_lambda<false, false>
{
    template <class Result, class Obj, class Func>
    static void call(Func &&func, Obj &, QFutureWatcher<Result> const *)
    {
        func();
    }
};

template <class Result, class Obj> struct invoke
{
    template <class Func> static void
    call(Func &&func, Obj &obj, QFutureWatcher<Result> const *watcher)
    {
        call_lambda<takes_obj_as_first_parameter<Func, Obj, Result>(), true>::
                call(std::forward<Func>(func), obj, watcher);
    }

    template <class FnArg> static void
    call(void (Obj::*member)(FnArg),
         Obj &obj,
         QFutureWatcher<Result> const *watcher)
    {
        (obj.*member)(watcher->result());
    }
};

template <class Obj> struct invoke<void, Obj>
{
    template <class Func> static void
    call(Func &&func, Obj &obj, QFutureWatcher<void> const *watcher)
    {
        call_lambda<takes_obj_as_first_parameter<Func, Obj>(), false>::call(
                std::forward<Func>(func),
                obj,
                watcher);
    }

    static void
    call(void (Obj::*member)(),
         Obj &obj,
         QFutureWatcher<void> const * /* watcher */)
    {
        (obj.*member)();
    }
};

}}  // namespace QtPdCom::details

#endif  // QTPDCOM_FUTUREWATCHERS_DETAILS_H
