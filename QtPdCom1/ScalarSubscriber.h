/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_SCALARSUBSCRIBER_H
#define QTPDCOM_SCALARSUBSCRIBER_H

#include "Transmission.h"
#include "Export.h"

#include <pdcom5/Variable.h>
#include <pdcom5/Subscription.h>

#include <QDebug> // qWarning()

namespace QtPdCom {

/****************************************************************************/

/** Subscriber of a single scalar value.
 */
class QTPDCOM_PUBLIC ScalarSubscriber
{
    public:
        ScalarSubscriber();
        virtual ~ScalarSubscriber();

        /** Subscribe to a process variable via variable.
         */
        void setVariable(
                PdCom::Variable pv, /**< Process variable. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const Transmission & = event_mode, /**< Transmission
                                                     details. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );

        /** Subscribe to a process variable via process and path.
         */
        void setVariable(
                PdCom::Process *process, /**< Process. */
                const QString &path, /**< Variable path. */
                const PdCom::Selector &selector = {}, /**< Selector. */
                const Transmission & = event_mode, /**< Transmission
                                                     details. */
                double scale = 1.0, /**< Scale factor. */
                double offset = 0.0, /**< Offset (applied after scaling). */
                double tau = 0.0 /**< PT1 filter time constant. A value less
                                    or equal to 0.0 means, that no filter is
                                    applied. */
                );
        void clearVariable();
        bool hasVariable() const; /**< Subscription active. */

        virtual void newValues(std::chrono::nanoseconds) = 0;
        virtual void stateChange(PdCom::Subscription::State);

        template <class T> void writeValue(T);

        double getFilterConstant() const;

        PdCom::Variable getVariable() const;
        const void *getData() const;

        /** Poll an active subscription.
         *
         * \return false if variable could not be polled.
         *
        */
        Q_INVOKABLE bool poll();

    protected:
        double scale;
        double offset;

        const PdCom::Selector &getSelector() const;

    private:
        struct Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;

        ScalarSubscriber(const ScalarSubscriber &); // not to be used
};

/****************************************************************************/

/** Write a value to the process.
 *
 * This is a convenience function, that checks for the subscription, before
 * writing the value.
 */
template <class T>
void ScalarSubscriber::writeValue(T value)
{
    if (not hasVariable()) {
        qWarning() << "ScalarSubscriber::writeValue(): Not subscribed!";
        return;
    }

    if (scale == 0.0) {
        qWarning() << "Avoiding division by zero scale.";
        return;
    }

    getVariable().setValue((value - offset) / scale, getSelector());
}

/****************************************************************************/

} // namespace

#endif
