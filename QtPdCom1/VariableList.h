/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_VARIABLELIST_H
#define QTPDCOM_VARIABLELIST_H

#include <QString>
#include <QVector>
#include <pdcom5/Variable.h>

namespace QtPdCom {

struct VariableList {
    QVector<QString> directories;
    QVector<PdCom::Variable> variables;
};

}


#endif  // QTPDCOM_VARIABLELIST_H
