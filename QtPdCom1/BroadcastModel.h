/*****************************************************************************
 *
 * Copyright (C) 2009-2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef BROADCASTMODEL_H
#define BROADCASTMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QString>

#include "Export.h"
#include "Process.h"

namespace QtPdCom {

class BroadcastModelPrivate;

/**
 * \brief Model for capturing broadcast messages.
 *
 * It contains three columns (date, message, username).
 * Please note that broadcasts have to be enabled in pdserv.
 */
class QTPDCOM_PUBLIC BroadcastModel: public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(QtPdCom::Process *process READ getProcess WRITE connectProcess)

  public:
    explicit BroadcastModel(QObject *parent = nullptr);
    ~BroadcastModel();

    int rowCount(const QModelIndex & = {}) const override;
    int columnCount(const QModelIndex & = {}) const override;
    QVariant
    data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(
            int section,
            Qt::Orientation orientation,
            int role = Qt::DisplayRole) const override;

    /** Connect to a Process.
     *
     * The old Process will be disconnected.
     * \param process New Process.
     */
    void connectProcess(QtPdCom::Process *process);
    QtPdCom::Process *getProcess() const;
    /**
     * Clear all stored broadcasts.
     */
    Q_INVOKABLE void clear();

    enum Roles {
        DateStringRole = Qt::UserRole + 1,
        MessageStringRole,
        UsernameRole,
    };
    Q_ENUM(Roles);
    QHash<int, QByteArray> roleNames() const override;

  private:
    Q_DECLARE_PRIVATE(BroadcastModel);

    QScopedPointer<BroadcastModelPrivate> const d_ptr;
};

}  // namespace QtPdCom


#endif  // BROADCASTMODEL_H
