/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_PROCESS_H
#define QTPDCOM_PROCESS_H

#include <QObject>
#include <QString>
#include <QList>
#include <QSslCertificate>
#include <QUrl>

#include <pdcom5.h>
#include <pdcom5/ClientStatistics.h>
#include <pdcom5/Process.h>
#include <QFuture>

#ifndef PDCOM_VERSION_CODE
# error "No PDCOM_VERSION_CODE found."
#elif \
    !PDCOM_DEVEL \
    && (PDCOM_VERSION_CODE < PDCOM_VERSION(5, 0, 0) \
    || PDCOM_VERSION_CODE >= PDCOM_VERSION(6, 0, 0))
# error "Invalid PdCom version."
#endif

#include "QtPdCom1.h"
#include "Export.h"
#include "FutureWatchers.h"
#include "VariableList.h"

class QTranslator;
class QSslKey;


#ifdef QTPDCOM_HAS_LOGIN_MANAGER
#include "LoginManager.h"
#else
namespace QtPdCom {
class LoginManager;
}

Q_DECLARE_OPAQUE_POINTER(QtPdCom::LoginManager*);
#endif

namespace QtPdCom {

/****************************************************************************/

/** PdCom::Process implementation for Qt.
 */
class QTPDCOM_PUBLIC Process:
    public QObject, public PdCom::Process
{
    Q_OBJECT
    Q_PROPERTY(bool connected READ isConnected NOTIFY connectionStatusChanged)
    Q_PROPERTY(ConnectionState connectionState READ getConnectionState NOTIFY connectionStatusChanged)
    Q_PROPERTY(SslCaMode sslCaMode READ getCaMode WRITE setCaMode NOTIFY sslCaModeChanged)
    Q_PROPERTY(int port READ getPort NOTIFY connectionStatusChanged)
    Q_PROPERTY(QString host READ getHost NOTIFY connectionStatusChanged)
    Q_PROPERTY(QUrl url READ getUrl NOTIFY connectionStatusChanged)
    Q_PROPERTY(QString applicationName READ getApplicationName WRITE setApplicationName)
    Q_PROPERTY(QtPdCom::LoginManager * loginManager READ getLoginManager WRITE setLoginManager)
    Q_PROPERTY(QVariant name READ nameQt NOTIFY connectionStatusChanged)
    Q_PROPERTY(QVariant version READ versionQt NOTIFY connectionStatusChanged)

    public:
        enum class SslCaMode {
            NoTLS, /**< Disable TLS, unencrypted traffic only. */
            DefaultCAs /**< Use system-default Certificate Authories. */,
            CustomCAs /**< Use provided CAs only. */,
            IgnoreCertificate /**< Accept any server certificate. */,
        };
        Q_ENUM(SslCaMode)

        Process(QObject *parent = nullptr);
        virtual ~Process();

        void setApplicationName(const QString &);
        QString getApplicationName() const;
        Q_INVOKABLE void connectToHost(const QString &, quint16 = 2345);
        Q_INVOKABLE void disconnectFromHost();

        /** State of the process connection.
         */
        enum ConnectionState {
            Disconnected, /**< Process disconnected. */
            Connecting, /**< Currently connecting. */
            Connected, /**< Process connection established. */
            ConnectError, /**< An error happened while connecting. */
            ConnectedError /**< An error happened, after the connection was
                             established. */
        };
        Q_ENUM(ConnectionState);

        ConnectionState getConnectionState() const;
        bool isConnected() const;
        const QString &getErrorString() const;
        QString getPeerName() const;
        QUrl getUrl() const;
        int getPort() const;
        QString getHost() const;

        Q_INVOKABLE void sendBroadcast(const QString &, const QString &attr = "text");
        quint64 getRxBytes() const;
        quint64 getTxBytes() const;

        /** Remote process name string */
        QVariant nameQt() const;
        /** Remote process version string */
        QVariant versionQt() const;

        /** Find a Variable.
         *
         * Finding a variable is asynchronous in general. If you need to write
         * to a variable, a variable object is needed. To write a variable
         * with a single call you can use futures and a lambda function. See
         * the example in the overloaded method
         * find(const QString&, Class *, Function&&).
         *
         * @return A Future. You can use VariableWatcher to monitor it.
         */
        QFuture<PdCom::Variable> find(const QString&);
        QFuture<VariableList> list(const QString& = "");
        QFuture<void> pingQt();
        QFuture<std::vector<PdCom::ClientStatistics>> getClientStatisticsQt();

        /** Also find a Variable.
         *
         * Here, a FutureWatcher is created on your behalf.
         * Callback can be one of
         *  - a member Function of obj which takes a PdCom::Variable
         *  - a lambda which takes a reference to obj and a PdCom::Variable
         *  - a lambda which takes only a PdCom::Variable
         *
         * The obj reference is needed to use it as a parent for the watcher
         * and to make the connections disconnect automatically when the Process
         * or the object itself goes away.
         *
         * Below is an example of how to make use of a lambda function to find
         * a variable and write to it once it is found:
         *
         * \code{.cpp}
         * const double val = 30.0;
         * const QString path = "/osc/amplitude/Setpoint";
         *
         * process->find(path, this, [val](const PdCom::Variable& var) {
         *     if (var.empty()) {
         *         std::cerr << "Variable not found." << std::endl;
         *     }
         *     else {
         *         var.setValue(val);
         *         std::cerr << "Set " << var.getPath()
         *                   << " to " << val << std::endl;
         *     }
         * });
         * \endcode
         *
         * \param path Variable to find.
         * \param obj QObject-derived class.
         * \param callback Callback which receives the result.
         * \return A freshly created FutureWatcher instance with all signals connected.
         */
        template<class Class, class Function>
        QFutureWatcher<PdCom::Variable>& find(const QString& path, Class *obj, Function&& callback);
        template<class Class, class Function>
        QFutureWatcher<VariableList>& list(const QString& path, Class *obj, Function&& callback);
        template<class Class, class Function>
        QFutureWatcher<void>& ping(Class *obj, Function&& callback);
        template<class Class, class Function>
        QFutureWatcher<std::vector<PdCom::ClientStatistics>>&
        getClientStatistics(Class *obj, Function&& callback);

        /**
         * Set Traffic Encryption mode.
         *
         * See the SslCaMode enum for details.
         *
         * \param mode Mode.
         */
        void setCaMode(SslCaMode mode);
        /**
         * Get Traffic Encryption mode.
         *
         * \return Traffic Encryption mode.
         */
        SslCaMode getCaMode() const;

        /**
         * Provide a client certificate.
         *
         * \param cert Public certificate.
         * \param key  Private key.
         */
        void setClientCertificate(const QSslCertificate& cert,const QSslKey& key);
        /**
         * Set list of trusted Certificate Authorities.
         *
         * For \c SslCaMode::CustomCAs mode.
         *
         * \param cas List of trusted CAs.
         */
        void setCustomCAs(QList<QSslCertificate> cas);

        static QtPdCom::Process *getDefaultProcess();
        static void setDefaultProcess(QtPdCom::Process *);

        PdCom::MessageManagerBase *getMessageManager() const;

        /**
         * Set the Login Manager.
         * \param lm The Login manager. May be \c NULL.
        */
        void setLoginManager(LoginManager* lm);
        LoginManager *getLoginManager() const;
    private:
        struct Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;

        /** Virtual from PdCom::Process. */
        std::string applicationName() const override;
        std::string hostname() const override;
        int read(char *, int) override;
        void write(const char *, size_t) override;
        void flush() override;
        void connected() override;
        void broadcastReply(
                const std::string &message,
                const std::string &attr,
                std::chrono::nanoseconds time_ns,
                const std::string &user) override;
        void pingReply() override;
        void findReply(PdCom::Variable const& var) override;
        void listReply(std::vector<PdCom::Variable> vars, std::vector<std::string> dirs) override;
        void clientStatisticsReply(std::vector<PdCom::ClientStatistics> statistics) override;
        void reset();

        /** Disconnect method inherited from QObject.
         *
         * This is made private, to avoid confusion.
         */
        bool disconnect(
                const char *signal = 0, /**< Signal. */
                const QObject *receiver = 0, /**< Receiver. */
                const char *method = 0 /**< Method. */
                );

        // make setMessageManager() private
        using PdCom::Process::setMessageManager;

    signals:
        /** Connection established.
         *
         * This is emitted after the connection is established.
         */
        void processConnected();

        /** Disconnected gracefully.
         *
         * This is only emitted, after the user called disconnectFromHost().
         */
        void disconnected();

        /** Connection error.
         *
         * This is emitted after a connection error or when the connection was
         * closed due to a parser error.
         */
        void error();

        void broadcastReceived(
                const QString &message,
                const QString &attr,
                std::uint64_t time_ns,
                const QString &user);

        void connectionStatusChanged();

        void sslCaModeChanged();


    private slots:
        void socketConnected();
        void socketDisconnected();
        void socketError();
        void socketRead();
};

/****************************************************************************/

template<class Class, class Function>
inline QFutureWatcher<PdCom::Variable>& Process::find(
    const QString& path, Class *obj, Function&& callback)
{
    auto& ans = createWatcher<PdCom::Variable>(obj, callback);
    ans.setFuture(find(path));
    return ans;
}

/****************************************************************************/

template<class Class, class Function>
inline QFutureWatcher<VariableList>& Process::list(const QString& path, Class *obj, Function&& callback)
{
    auto& ans = createWatcher<VariableList>(obj, callback);
    ans.setFuture(list(path));
    return ans;
}

/****************************************************************************/

template<class Class, class Function>
inline QFutureWatcher<void>& Process::ping(Class *obj, Function&& callback)
{
    auto& ans = createWatcher<void>(obj, callback);
    ans.setFuture(pingQt());
    return ans;
}

/****************************************************************************/

template<class Class, class Function>
inline QFutureWatcher<std::vector<PdCom::ClientStatistics>>&
Process::getClientStatistics(Class *obj, Function&& callback)
{
    auto& ans = createWatcher<std::vector<PdCom::ClientStatistics>>(
        obj, callback
    );
    ans.setFuture(getClientStatisticsQt());
    return ans;
}

/****************************************************************************/

} // namespace

#endif  // QTPDCOM_PROCESS_H
