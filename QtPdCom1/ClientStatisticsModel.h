/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_CLIENTSTATISTICSMODEL_H
#define QTPDCOM_CLIENTSTATISTICSMODEL_H

#include <QAbstractTableModel>
#include "Export.h"
// moc needs full definition of Process to register the metatype for the
// property automatically. See https://bugreports.qt.io/browse/QTBUG-50242
#include "Process.h"

namespace QtPdCom {

class ClientStatisticsModelPrivate;

class QTPDCOM_PUBLIC ClientStatisticsModel: public QAbstractTableModel
{
    Q_OBJECT
    Q_PROPERTY(QtPdCom::Process *process READ getProcess WRITE setProcess)

  public:
    explicit ClientStatisticsModel(QObject *parent = nullptr);
    virtual ~ClientStatisticsModel();


    int rowCount(const QModelIndex &) const override;
    int columnCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex &, int) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role)
            const override;

    /**
     * Update statistics from server.
     */
    Q_INVOKABLE void poll();
    /**
     * Clear stored statistics.
     */
    Q_INVOKABLE void clear();
    void setProcess(QtPdCom::Process *);
    QtPdCom::Process *getProcess() const;

    QHash<int, QByteArray> roleNames() const override;

    enum Roles {
        NameRole = Qt::UserRole + 1,
        ApplicationNameRole,
        RxByteRole,
        TxByteRole,
        ConnectedTimeRole,
    };
    Q_ENUM(Roles)


  private:
    Q_DECLARE_PRIVATE(ClientStatisticsModel)
    Q_DISABLE_COPY(ClientStatisticsModel)

    QScopedPointer<ClientStatisticsModelPrivate> d_ptr;
};

}  // namespace QtPdCom


#endif  // QTPDCOM_CLIENTSTATISTICSMODEL_H
