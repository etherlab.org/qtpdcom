/*****************************************************************************
 *
 * Copyright (C) 2009-2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_LOGINMANAGER_H
#define QTPDCOM_LOGINMANAGER_H

#include "Export.h"

#include <QObject>
#include <QScopedPointer>
#include <QString>

namespace QtPdCom {

class LoginManagerPrivate;
class Process;

/**
 * Class to handle the Authentication process.
 *
 * Prior to using an instance of this class,
 * InitLibrary() has to be called.
 *
 * In Qt6 QML, simply create an \c SaslInitializer instance
 * before connecting to a process.
 *
 */
class QTPDCOM_PUBLIC LoginManager: public QObject
{
    Q_OBJECT

  public:
    explicit LoginManager(QString server_name = QString(), QObject *parent = nullptr);
    ~LoginManager();

    /**
     * Set Login Name.
     *
     * \param name Login name.
     */
    Q_INVOKABLE void setAuthName(QString name);
    /**
     * Set Password.
     *
     * \param password Password.
     */
    Q_INVOKABLE void setPassword(QString password);

    /**
     * Clear stored credentials.
     *
     */
    Q_INVOKABLE void clearCredentials();

    /**
     * Start the login process.
     *
     * If possible, please call setAuthName() and
     * setPassword() in advance.
     * Do not call login() from whithin any Authentification-related
     * callback (i.e. slots connected to any of our signals which are not
     * queued connections) as use-after-free can happen as a new session is
     * started.
     */
    Q_INVOKABLE void login();
    /**
     * logout.
     */
    Q_INVOKABLE void logout();

    /* Retrieve reason for login failure.
     */
    Q_INVOKABLE QString getErrorMessage();

    /** Sasl global initialization.
     *
     * Call this at startup of your application to initialize the underlying
     * sasl library.
     *
     * \param plugin_path Path to SASL.
     * \throws PdCom::Exception Initialization failed.
     */
    static void InitLibrary(const char *plugin_path = nullptr);
    /** Sasl global finalization
     */
    static void FinalizeLibrary();

  signals:

    /**
     * Username and/or password has not been set.
     */
    void needCredentials();
    /**
     * Authentification was successful.
     */
    void loginSuccessful();
    /**
     * Authentification was not successful.
     * See getErrorMessage().
     */
    void loginFailed();

  private:
    Q_DECLARE_PRIVATE(LoginManager);
    Q_DISABLE_COPY(LoginManager);
    LoginManager& operator=(LoginManager&&) = delete;
    LoginManager(LoginManager&&) = delete;

    QScopedPointer<LoginManagerPrivate> d_ptr;
    friend Process;
};

}  // namespace QtPdCom

#endif  // QTPDCOM_LOGINMANAGER_H
