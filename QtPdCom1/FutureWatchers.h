/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/


#ifndef QTPDCOM_FUTUREWATCHERS_H
#define QTPDCOM_FUTUREWATCHERS_H

#include <QFutureWatcher>
#include <pdcom5/Variable.h>
#include "VariableList.h"
#include "Export.h"
#include "FutureWatchersDetails.h"


namespace QtPdCom {

/**
 * Convenience class for processing the result of Process::findQt()
 *
 * Let's say you have a Widget which needs to process a PdCom::Variable
 * instance.
 *
 *
 * @code
 *
 * class MyWidget : public QObject
 * {
 *     Q_OBJECT
 *
 *   public:
 *     MyWidget(QObject *parent) : QObject(parent) {}
 *
 *     void setVariable(QtPdCom::Process& proc, QString path)
 *     {
 *         const auto watcher = new QtPdCom::VariableWatcher(this);
 *         // this will be fired when the result has arrived
 *         connect(watcher, &QtPdCom::VariableWatcher::finished,
 *                 this, &MyWidget::onVariableFound);
 *         // delete watcher if Process is resetted in the meantime
 *         connect(watcher, &QtPdCom::VarableWatcher::cancelled,
 *                 watcher, &QObject::deleteLater);
 *         // start watching
 *         watcher->setFuture(proc.find(path));
 *     }
 *
 *   private slots:
 *     void onVariableFound()
 *     {
 *         const auto watcher =
 * qobject_cast<QtPdCom::FindVariableWatcher*>(QObject::sender()); if
 * (!watcher) return;
 *         // fetch result from watcher
 *         const PdCom::Variable result = watcher->result();
 *         watcher->deleteLater();
 *         // handle result
 *     }
 * };
 *
 *
 * @endcode
 *
 *
 */
class QTPDCOM_PUBLIC VariableWatcher: public QFutureWatcher<PdCom::Variable>
{
    Q_OBJECT

  public:
    using QFutureWatcher<PdCom::Variable>::QFutureWatcher;
};

class QTPDCOM_PUBLIC ListWatcher: public QFutureWatcher<VariableList>
{
    Q_OBJECT

  public:
    using QFutureWatcher<VariableList>::QFutureWatcher;
};


template< class Result,class Object, class Callback>
inline QFutureWatcher<Result>& createWatcher(Object *obj, Callback &&callback)
{
    const auto watcher = new QFutureWatcher<Result>(obj);
    QObject::connect(
            watcher,
            &QFutureWatcherBase::finished,
            obj,
            [callback, watcher, obj]() {
                details::invoke<Result, Object>::call(callback, *obj, watcher);
                watcher->deleteLater();
            });
    QObject::connect(
            watcher,
            &QFutureWatcherBase::canceled,
            watcher,
            &QObject::deleteLater);
    return *watcher;
}


}  // namespace QtPdCom

#endif  // QTPDCOM_FUTUREWATCHERS_H
