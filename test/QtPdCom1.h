/*****************************************************************************
 * vim:tw=78
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/** @file */

#ifndef QTPDCOM1_H
#define QTPDCOM1_H

#include "QtPdCom1/Export.h"

/// QtPdCom major version
#define QTPDCOM_MAJOR 0
/// QtPdCom minor version
#define QTPDCOM_MINOR 0
/// Patch level of release. Note this may be a string
#define QTPDCOM_RELEASE 0

/// Macro to generate a version code for comparison
#define QTPDCOM_VERSION(A, B, C) (((A) << 16) + ((B) << 8) + (C))

/// Current QtPdCom version
#define QTPDCOM_VERSION_CODE \
        QTPDCOM_VERSION(QTPDCOM_MAJOR, QTPDCOM_MINOR, QTPDCOM_RELEASE)

namespace QtPdCom {
/// Library version string as "major.minor.patch".
QTPDCOM_PUBLIC extern const char* const qtpdcom_version_code;
/** Library version with four fields followed by the commit hash.
 * A trailing plus sign shows that uncommitted changes existed during
 * the build. The hash is prefixed with a "g" to indicate that git is used
 * as version control system.
 *
 * Example: "5.1.0.8.gea62937+".
 */
QTPDCOM_PUBLIC extern const char* const qtpdcom_full_version;
}

#endif // QTPDCOM1_H
