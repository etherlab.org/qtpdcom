#-----------------------------------------------------------------------------
#
# Copyright (C) 2009 - 2025  Florian Pose <fp@igh.de>
#
# This file is part of the QtPdCom library.
#
# The QtPdCom library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# The QtPdCom library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
#
# vim: syntax=config
#
#-----------------------------------------------------------------------------

TEMPLATE = app
TARGET = test
QT += network xml testlib
CONFIG += debug c++14

# yes, we're hiding symbols in an executable. Otherwise gcc complains about
# "... declared with greater visibility than the type of its field ..."
CONFIG += hide_symbols

#-----------------------------------------------------------------------------

DEPENDPATH += ..
INCLUDEPATH += ../QtPdCom1 . ..

#-----------------------------------------------------------------------------

QMAKE_CXXFLAGS_DEBUG -= -O2
QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS += --coverage -g -O0

QMAKE_LFLAGS += -L$$OUT_PWD/..
QMAKE_LFLAGS_APP += -Wl,--rpath -Wl,..

LIBS += -lgcov

DEFINES += PD_DEBUG_MESSAGE_MODEL

#-----------------------------------------------------------------------------

HEADERS += \
    ../QtPdCom1/Message.h \
    ../QtPdCom1/MessageModel.h \
    ../QtPdCom1/Process.h \
    ../QtPdCom1/ScalarSubscriber.h \
    ../QtPdCom1/ScalarVariable.h \
    ../QtPdCom1/Transmission.h \
    ../src/MessageImpl.h \
    ../src/MessageItem.h \
    ../src/MessageManager.h \
    ../src/MessageModelImpl.h \
    MessageModelTest.h \
    pdcom5/Exception.h \
    pdcom5/MessageManagerBase.h \
    pdcom5/Process.h \
    pdcom5/Selector.h \
    pdcom5/SizeTypeInfo.h \
    pdcom5/Subscriber.h \
    pdcom5/Subscription.h \
    pdcom5/Variable.h

SOURCES += \
    ../src/Message.cpp \
    ../src/MessageImpl.cpp \
    ../src/MessageItem.cpp \
    ../src/MessageManager.cpp \
    ../src/MessageModel.cpp \
    ../src/MessageModelImpl.cpp \
    ../src/Process.cpp \
    ../src/ScalarSubscriber.cpp \
    ../src/Transmission.cpp \
    MessageModelTest.cpp \
    pdcom5/MessageManagerBase.cpp \
    pdcom5/MockProcess.cpp \
    pdcom5/Selector.cpp \
    pdcom5/Subscriber.cpp \
    pdcom5/Subscription.cpp \
    pdcom5/Variable.cpp

# translations ---------------------------------------------------------------

CONFIG += lrelease embed_translations
TRANSLATIONS = \
    ../QtPdCom_de.ts \
    ../QtPdCom_nl.ts

QM_FILES_RESOURCE_PREFIX = /QtPdCom
# make qmake to name the translation resource file the same as with cmake
# this is needed because Q_INIT_RESOURCE needs to be called
QMAKE_RESOURCE_FLAGS = -name QtPdCom_ts
CODECFORTR = UTF-8

#-----------------------------------------------------------------------------
