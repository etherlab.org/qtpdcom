[![pipeline status](https://gitlab.com/etherlab.org/qtpdcom/badges/main/pipeline.svg)](https://gitlab.com/etherlab.org/qtpdcom/-/commits/main)
[![coverage report](https://gitlab.com/etherlab.org/qtpdcom/badges/main/coverage.svg)](https://docs.etherlab.org/qtpdcom/1/coverage/index.html)
[![online documentation](https://img.shields.io/badge/Documentation-HTML-informational)](https://docs.etherlab.org/qtpdcom/1/doxygen/index.html)

# General Information

This is the QtPdCom library, a Qt implementation of PdCom 5
from the EtherLab project. See https://gitlab.com/etherlab.org/qtpdcom

# License

The QtPdCom library is licensed unter the terms and conditions of the GNU
Lesser General Public License (LGPL), version 3 or (at your option) any later
version.

# Prerequisites

- Qt5 or Qt6
- PdCom 5 (see https://gitlab.com/etherlab.org/pdcom)

# Building and Installing

This library is built using cmake.

```
mkdir build && cd build
cmake ..
make install
```
To use Qt6, set `USE_QT6` to `ON`.
Note that Qt6 requires at least GCC9, so you likely want to set `CC=gcc-9` and `CXX=g++-9`.
With GCC7, `<filesystem>` header which is included in `<QFile>` cannot be found.

If you want to embed the Qt major version number into the QtPdCom library name,
which is useful to have parallel installations for Qt5 and Qt6,
set `PUT_QT_MAJOR_INTO_LIBNAME` to `ON`.
`libQtPdCom1.so` then becomes `libQt5PdCom1.so`.

To change the installation path, call cmake with `-DCMAKE_INSTALL_PREFIX=/my/custom/prefix`.
If PdCom5 is installed at a non-standard location,
use `-DCMAKE_PREFIX_PATH=/pdcom/install/prefix`.
You can even use this option to point CMake to a build directory of PdCom5,
this adds the possibility to use a version of PdCom5 which is not installed at all.

To import this library into your own application,
add the following to your `CMakeLists.txt`:
```cmake
find_package(QtPdCom1 REQUIRED)
target_link_libraries(your_application PUBLIC
   EtherLab::QtPdCom1
)
```
All dependencies are imported and linked automatically.
The `CMAKE_PREFIX_PATH` argument works here, too.

# Classes exported in QML

For Qt5, please have a look at [Qt5 QML Process-Data Widgets library](https://gitlab.com/etherlab.org/pd-qml-widgets).

In Qt6, the following classes are available with `import de.igh.qtpdcom 1.4`:
 - QtPdCom::ClientStatisticsModel
 - QtPdCom::MessageModel
 - QtPdCom::LoginManager
 - QtPdCom::Process
 - SaslInitializer, needed for QtPdCom::LoginManager
 - QtPdCom::PdVariable (named as `PdVariable`), for PdCom Signals and Parameters

For convenience, a default QtPdCom::Process instance is created for you
as a singleton `DefaultProcess`.
This instance is taken by the widgets if no process instance is set.
Use `DefaultProcess.connectToHost("localhost", 2345)` in your QML application
to connect this default instance.

The `find_package()` call in CMake populates `QTPDCOM_QML_ROOT_PATH`
which points to the root of the installed qml files
if `COMPONENTS Qml` is supplied to `find_package()`.
In most cases, this will be something like
`/usr/lib64/qt6/qml/`.
You can use this variable to populate `IMPORT_PATH` argument for
your `qt_add_qml_module()`.

Have fun!
