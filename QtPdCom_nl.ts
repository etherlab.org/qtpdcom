<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL" sourcelanguage="en_US">
<context>
    <name>QtPdCom::BroadcastModel</name>
    <message>
        <location filename="src/BroadcastModel.cpp" line="119"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="src/BroadcastModel.cpp" line="121"/>
        <source>Message</source>
        <translation>Melding</translation>
    </message>
    <message>
        <location filename="src/BroadcastModel.cpp" line="123"/>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
</context>
<context>
    <name>QtPdCom::ClientStatisticsModel</name>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="110"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="112"/>
        <source>Application</source>
        <translation>Programma</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="114"/>
        <source>Received bytes</source>
        <translation>Ontvangen bytes</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="116"/>
        <source>Sent bytes</source>
        <translation>Verzonden bytes</translation>
    </message>
    <message>
        <location filename="src/ClientStatisticsModel.cpp" line="118"/>
        <source>Connected time</source>
        <translation>verbonden tijd</translation>
    </message>
</context>
<context>
    <name>QtPdCom::LoginManager</name>
    <message>
        <location filename="src/LoginManager.cpp" line="148"/>
        <source>Login successful.</source>
        <translation>Inloggen succesvol.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="150"/>
        <source>Login failed: Please check the given username and password.</source>
        <translation>Inloggen mislukt: Controleer de opgegeven gebruikersnaam en wachtwoord.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="153"/>
        <source>Login was aborted.</source>
        <translation>Inloggen is afgebroken.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="155"/>
        <source>Login failed: No authentication mechanism found.</source>
        <translation>Inloggen mislukt: Geen authenticatiemechanisme gevonden.</translation>
    </message>
    <message>
        <location filename="src/LoginManager.cpp" line="157"/>
        <source>Unknown login error</source>
        <translation>Onbekende inlogfout</translation>
    </message>
</context>
<context>
    <name>QtPdCom::MessageModel</name>
    <message>
        <location filename="src/MessageModel.cpp" line="69"/>
        <source>Failed to open %1.</source>
        <translation>Openen van %1 mislukt.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="79"/>
        <location filename="src/MessageModel.cpp" line="92"/>
        <source>Failed to parse %1, line %2, column %3: %4</source>
        <translation>Parsen van %1, lijn %2, kolom %3 mislukt: %4</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="107"/>
        <source>Failed to process %1: No plain message file (%2)!</source>
        <translation>Verwerken van %1 mislukt: Geen vlakke meldingsfile gevonden (%2)!</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="225"/>
        <source>Failed to connect to message manager.</source>
        <translation>Verbinding met meldings manager mislukt.</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="251"/>
        <source>Failed to subscribe to %1: %2</source>
        <translation>Kon geen abonnement nemen op %1: %2</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="425"/>
        <source>Message</source>
        <translation>Melding</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="428"/>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <location filename="src/MessageModel.cpp" line="431"/>
        <source>Reset</source>
        <translation>Gekwiteerd</translation>
    </message>
</context>
</TS>
