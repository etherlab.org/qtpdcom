/*****************************************************************************
 *
 * Copyright (C) 2009 - 2024  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageModelFilter.h"
#include "MessageModel.h"

namespace QtPdCom {

class MessageModelFilterPrivate
{
    public:
        bool showOnlyActiveMessages = false;
};
}  // namespace QtPdCom

using QtPdCom::MessageModelFilter;

MessageModelFilter::MessageModelFilter(QObject *parent):
    QSortFilterProxyModel(parent),
    d_ptr(new QtPdCom::MessageModelFilterPrivate())
{}

MessageModelFilter::~MessageModelFilter() = default;

bool MessageModelFilter::getShowOnlyActiveMessages() const
{
    const Q_D(MessageModelFilter);
    return d->showOnlyActiveMessages;
}

void MessageModelFilter::setShowOnlyActiveMessages(bool value)
{
    Q_D(MessageModelFilter);
    d->showOnlyActiveMessages = value;
}

bool MessageModelFilter::filterAcceptsRow(
        int sourceRow,
        const QModelIndex &sourceParent) const
{
    Q_ASSERT(sourceModel());
    const Q_D(MessageModelFilter);
    const auto idx = sourceModel()->index(sourceRow, 0, sourceParent);
    return !d->showOnlyActiveMessages
            || sourceModel()
                       ->data(idx, MessageModel::ResetTimeStringRole)
                       .toString()
                       .isEmpty();
}
