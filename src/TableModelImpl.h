/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_TABLEMODEL_IMPL_H
#define QTPDCOM_TABLEMODEL_IMPL_H

#include <QtPdCom1/TableModel.h>

#include <QAbstractTableModel>
#include <QVector>
#include <QColor>

#define DEFAULT_DECIMALS 15
#define DEFAULT_HIGHLIGHT_COLOR QColor(152, 183, 255)
#define DEFAULT_DISABLED_COLOR QColor(220, 220, 220)

namespace QtPdCom {

/****************************************************************************/

/** Table model.
 *
 * \see TableColumn.
 */
class TableModel::Impl
{
    public:
        Impl(TableModel *parent):
            parent(parent)
        {}

    private:
        TableModel *const parent;
        unsigned int rows = 0;
        unsigned int visibleRows = UINT_MAX;
        unsigned int rowCapacity = 0;
        typedef QVector<TableColumn *> ColumnVector; /**< Column vector type.
                                                      */
        ColumnVector columnVector; /**< Vector of table columns. */

        void updateRows();
        void ensureEditing();

        QtPdCom::IntVariable valueHighlightRow;
        QtPdCom::IntVariable visibleRowCount;

        friend class TableModel;
};

/****************************************************************************/

class QtPdCom::TableColumn::Impl
{
        friend class TableColumn;

    public:
        Impl(TableColumn *parent, const QString &header);
        ~Impl();

        void stateChanged(PdCom::Subscription::State state);
        void newValues(std::chrono::nanoseconds)
        {
            dataPresent = true;
            emit parent->valueChanged();
        }

        void insertRow(int position, int count);
        void deleteRow(int position, int count);

        QString getRow(int row, const QLocale &locale) const;
        bool setRow(QString valueStr, int row, const QLocale &locale);

        void ensureEditData();

    private:
        TableColumn *parent; /**< Parent object */
        QString header;      /**< Table column header. */

        double scale;     /**< Scale factor for values. */
        double offset;    /**< Offset for values. */
        bool dataPresent; /**< Valid data have been received. */
        double *editData; /**< Temporary editing data. */
        bool enabled;     /**< Column enabled. */
        QHash<unsigned int, bool> enabledRows; /**< Enabled table rows. */
        int highlightRow;  /**< Index of the row to highlight, or -1. */
        quint32 decimals;  /**< Number of decimal digits. */
        double lowerLimit; /**< Lower limit for value of column (this is a
                              hint for the Input Method to limit the value).
                            */
        double upperLimit; /**< Upper limit for value of column (this is a
                              hint for the Input Method to limit the value).
                            */
        QColor highlightColor;
        QColor disabledColor;

        class Subscription;
        std::unique_ptr<Subscription> subscription;
};

/****************************************************************************/

}  // namespace QtPdCom

#endif
