/*****************************************************************************
 *
 * Copyright (C) 2018       Wilhelm Hagemeister <hm@igh.de>
 *               2021-2022  Florian Pose <fp@igh.de>
 *               2024       Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "PdVariable.h"
using QtPdCom::PdVariable;

#include <QtPdCom1/Transmission.h>

#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <QVector>
#include <QDebug>

namespace PdCom { namespace details {

template <class T> struct is_contiguous<QVector<T>>: std::true_type
{};
}}  // namespace PdCom::details

namespace {

PdCom::Selector getSelector(QtPdCom::PdConnection const &conn)
{
    bool ok = true;
    auto ans = conn.getSelector(&ok);
    if (!ok) {
        throw PdCom::Exception("Invalid Selector Fragment in Path");
    }
    return ans;
}

class VariantGenerator
{
    public:
        explicit VariantGenerator(
                PdCom::Variable var,
                const QtPdCom::PdConnection &conn):
            var_(var),
            conn_(conn)
        {
            if (const auto intvl = conn.getTransmission().getInterval()) {
                if (conn_.getTau()) {
                    filterConstant_ = intvl / conn_.getTau();
                }
            }
        }
        virtual QVariant getValue() const = 0;
        virtual void copyData(const PdCom::Subscription &sub) = 0;
        virtual bool
        setValue(QVariant const data, const PdCom::Selector &selector) = 0;
        virtual ~VariantGenerator() = default;
        PdCom::Variable const var_;
        const QtPdCom::PdConnection &conn_;
        double filterConstant_ = 1.0;

        template <typename T> void scaleToUser(T &old, const T newValue) const
        {
            const auto tmp = newValue * conn_.getScale() + conn_.getOffset();
            old = filterConstant_ * (tmp - old) + old;
        }

        template <typename T> T scaleToProcess(T value) const
        {
            if (conn_.getScale()) {
                return (value - conn_.getOffset()) / conn_.getScale();
            }
            else {
                return 0.0;
            }
        }
};

template <typename T> class ScalarVariantGenerator final:
    public VariantGenerator
{
        T value_ = static_cast<T>(0);

    public:
        explicit ScalarVariantGenerator(
                PdCom::Variable var,
                const QtPdCom::PdConnection &conn):
            VariantGenerator(var, conn)
        {}
        QVariant getValue() const override
        {
            return QVariant::fromValue<T>(value_);
        }
        void copyData(const PdCom::Subscription &sub) override
        {
            scaleToUser(value_, sub.getValue<T>());
        }
        bool setValue(QVariant const data, const PdCom::Selector &selector)
                override
        {
            if (data.canConvert<T>() && var_.isWriteable()) {
                var_.setValue(scaleToProcess(data.value<T>()), selector);
                return true;
            }
            return false;
        }
};

template <typename T> class VectorVariantGenerator final:
    public VariantGenerator
{
        QList<T> values_;
        QVector<T> write_values_;

    public:
        explicit VectorVariantGenerator(
                PdCom::Variable var,
                const QtPdCom::PdConnection &conn):
            VariantGenerator(var, conn)
        {
            values_.resize(var.getSizeInfo().totalElements());
            write_values_.resize(var.getSizeInfo().totalElements());
        }
        QVariant getValue() const override
        {
            return QVariant::fromValue(values_);
        }
        void copyData(const PdCom::Subscription &sub) override
        {
            Q_ASSERT(
                    sub.getVariable().getSizeInfo().totalElements()
                    == values_.size());
            size_t offset = 0;
            for (auto &value : values_) {
                T tmp;
                sub.getValue(tmp, offset);
                scaleToUser(value, tmp);
                ++offset;
            }
        }
        bool setValue(QVariant const data, const PdCom::Selector &selector)
                override
        {
            if (!data.canConvert<QVariantList>()) {
                return false;
            }
            const auto list = data.value<QVariantList>();
            if (list.size() != write_values_.size()) {
                return false;
            }
            for (size_t i = 0; i < write_values_.size(); ++i) {
                if (!list[i].canConvert<T>()) {
                    return false;
                }
                write_values_[i] = scaleToProcess(list[i].value<T>());
            }
            var_.setValue(write_values_, selector);
            return true;
        }
};

template <typename T> class MatrixVariantGenerator final:
    public VariantGenerator
{
        QList<QList<T>> values_;
        QVector<T> write_values_;

    public:
        explicit MatrixVariantGenerator(
                PdCom::Variable var,
                const QtPdCom::PdConnection &conn):
            VariantGenerator(var, conn)
        {
            const auto si = var.getSizeInfo();
            if (!si.is2DMatrix()) {
                throw std::invalid_argument("Not a 2D matrix");
            }
            values_.resize(si.rows());
            for (auto &row : values_) {
                row.resize(si.columns());
            }
            write_values_.resize(si.totalElements());
        }
        QVariant getValue() const override
        {
            return QVariant::fromValue(values_);
        }
        void copyData(const PdCom::Subscription &sub) override
        {
            const auto si = var_.getSizeInfo();
            Q_ASSERT(
                    si.is2DMatrix() && si.rows() == values_.size()
                    && values_.at(0).size() == si.columns());
            size_t offset = 0;
            for (auto &row : values_) {
                for (auto &cell : row) {
                    T tmp;
                    sub.getValue(tmp, offset);
                    scaleToUser(cell, tmp);
                    ++offset;
                }
            }
        }
        bool setValue(QVariant const data, const PdCom::Selector &selector)
                override
        {
            const auto si = var_.getSizeInfo();
            Q_ASSERT(si.totalElements() == write_values_.size());
            if (!var_.isWriteable() || !data.canConvert<QVariantList>()) {
                return false;
            }
            const auto rows = data.value<QVariantList>();
            if (rows.size() != si.rows()) {
                return false;
            }
            size_t offset = 0;
            for (const auto &row_v : rows) {
                if (!row_v.canConvert<QVariantList>()) {
                    return false;
                }
                const auto row = row_v.value<QVariantList>();
                if (row.size() != si.columns()) {
                    return false;
                }
                for (const auto &cell : row) {
                    if (!cell.canConvert<T>()) {
                        return false;
                    }
                    write_values_[offset] = cell.value<T>();
                    ++offset;
                }
            }
            var_.setValue(write_values_, selector);
            return true;
        }
};
}  // namespace

/****************************************************************************/

class QtPdCom::PdVariable::Impl
{
        friend class PdVariable;

    public:
        Impl(PdVariable *parent):
            parent(parent),
            pollOnce(false),
            polledOnce(false),
            dataPresent(false)
        {}

    private:
        PdVariable *parent;
        PdConnection pd_connection_;
        QMetaObject::Connection process_disconnected, process_error;
        std::chrono::nanoseconds mTime; /**< Modification Time. */
        bool pollOnce;
        bool polledOnce;
        bool dataPresent; /**< process values are valid. */

        class Subscription;
        std::unique_ptr<Subscription> subscription;
};

/****************************************************************************/

class QtPdCom::PdVariable::Impl::Subscription:
    public PdCom::Subscriber,
    PdCom::Subscription
{
        friend class PdVariable;

    public:
        Subscription(
                PdVariable::Impl *impl,
                PdCom::Variable pv,
                const QtPdCom::Transmission &transmission,
                PdCom::Selector selector):
            Subscriber(transmission.toPdCom()),
            PdCom::Subscription(*this, pv, selector),
            impl(impl),
            selector_(selector)
        {}

        Subscription(
                PdVariable::Impl *impl,
                PdCom::Process *process,
                const std::string &path,
                const QtPdCom::Transmission &transmission,
                PdCom::Selector selector):
            Subscriber(transmission.toPdCom()),
            PdCom::Subscription(*this, *process, path, selector),
            impl(impl),
            selector_(selector)
        {}

        QVariant getValue() const
        {
            if (!generator_) {
                return {};
            }
            return generator_->getValue();
        }

        bool setValue(QVariant const data)
        {
            if (!generator_) {
                return false;
            }
            return generator_->setValue(data, selector_);
        }

        void clearGenerator() { generator_.reset(); }

    private:
        PdVariable::Impl *const impl;
        std::unique_ptr<VariantGenerator> generator_;
        PdCom::Selector const selector_;

        void stateChanged(const PdCom::Subscription &sub) override
        {
            if (getState() != PdCom::Subscription::State::Active) {
                generator_.reset();
                const bool was_present = impl->dataPresent;
                impl->dataPresent = false;
                emit impl->parent->dataPresentChanged(impl->dataPresent);
                if (was_present) {
                    emit impl->parent->valueChanged();
                }
            }

            if (getState() == PdCom::Subscription::State::Active) {
                if (!generator_) {
                    bool ok = true;
                    const auto var = sub.getVariable();
                    const auto si = selector_.getViewSizeInfo(var);
                    const auto ti = var.getTypeInfo();
                    if (si.isScalar()) {
                        switch (ti.type) {
                            case PdCom::TypeInfo::boolean_T:
                                generator_.reset(
                                        new ScalarVariantGenerator<bool>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::uint8_T:
                            case PdCom::TypeInfo::uint16_T:
                            case PdCom::TypeInfo::uint32_T:
                            case PdCom::TypeInfo::uint64_T:
                                generator_.reset(
                                        new ScalarVariantGenerator<uint64_t>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::int8_T:
                            case PdCom::TypeInfo::int16_T:
                            case PdCom::TypeInfo::int32_T:
                            case PdCom::TypeInfo::int64_T:
                                generator_.reset(
                                        new ScalarVariantGenerator<int64_t>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::single_T:
                            case PdCom::TypeInfo::double_T:
                            default:
                                generator_.reset(
                                        new ScalarVariantGenerator<double>(
                                                var,
                                                impl->pd_connection_));
                                break;
                        }
                    }
                    else if (si.is2DMatrix()) {
                        switch (ti.type) {
                            case PdCom::TypeInfo::boolean_T:
                                generator_.reset(
                                        new MatrixVariantGenerator<bool>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::uint8_T:
                            case PdCom::TypeInfo::uint16_T:
                            case PdCom::TypeInfo::uint32_T:
                            case PdCom::TypeInfo::uint64_T:
                                generator_.reset(
                                        new MatrixVariantGenerator<uint64_t>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::int8_T:
                            case PdCom::TypeInfo::int16_T:
                            case PdCom::TypeInfo::int32_T:
                            case PdCom::TypeInfo::int64_T:
                                generator_.reset(
                                        new MatrixVariantGenerator<int64_t>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::single_T:
                            case PdCom::TypeInfo::double_T:
                            default:
                                generator_.reset(
                                        new MatrixVariantGenerator<double>(
                                                var,
                                                impl->pd_connection_));
                                break;
                        }
                    }
                    else {
                        switch (ti.type) {
                            case PdCom::TypeInfo::boolean_T:
                                generator_.reset(
                                        new VectorVariantGenerator<bool>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::uint8_T:
                            case PdCom::TypeInfo::uint16_T:
                            case PdCom::TypeInfo::uint32_T:
                            case PdCom::TypeInfo::uint64_T:
                                generator_.reset(
                                        new VectorVariantGenerator<uint64_t>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::int8_T:
                            case PdCom::TypeInfo::int16_T:
                            case PdCom::TypeInfo::int32_T:
                            case PdCom::TypeInfo::int64_T:
                                generator_.reset(
                                        new VectorVariantGenerator<int64_t>(
                                                var,
                                                impl->pd_connection_));
                                break;
                            case PdCom::TypeInfo::single_T:
                            case PdCom::TypeInfo::double_T:
                            default:
                                generator_.reset(
                                        new VectorVariantGenerator<double>(
                                                var,
                                                impl->pd_connection_));
                                break;
                        }
                    }
                }
                if (impl->pollOnce && !impl->polledOnce) {
                    impl->subscription
                            ->poll();  // poll once to get initial value
                    impl->polledOnce = true;
                }
            }
            if (getState() == PdCom::Subscription::State::Invalid) {
                impl->polledOnce = false;
            }
        }

        void newValues(std::chrono::nanoseconds ts) override
        {
            if (!generator_) {
                return;
            }

            generator_->copyData(*this);
            if (!impl->dataPresent) {
                impl->dataPresent = true;
                emit impl->parent->dataPresentChanged(impl->dataPresent);
            }
            impl->mTime = ts;
            emit impl->parent->valueChanged();
            emit impl->parent->valueUpdated(ts);
        }
};

/****************************************************************************/

/** Constructor.
 */

PdVariable::PdVariable(QObject *parent):
    QObject(parent),
    impl(std::unique_ptr<PdVariable::Impl>(new Impl(this)))
{}

/****************************************************************************/

/** Destructor.
 */
PdVariable::~PdVariable()
{
    clearVariable();
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void PdVariable::updateConnection()
{
    clearVariable();

    if (impl->process_disconnected) {
        QObject::disconnect(impl->process_disconnected);
    }

    if (impl->process_error) {
        QObject::disconnect(impl->process_error);
    }

    if (!impl->pd_connection_.getProcess()
        || impl->pd_connection_.getPath().isEmpty()) {
        return;
    }


    impl->process_disconnected =
            connect(impl->pd_connection_.getProcess(),
                    &Process::disconnected,
                    this,
                    &PdVariable::clearData);
    impl->process_error =
            connect(impl->pd_connection_.getProcess(),
                    &Process::error,
                    this,
                    &PdVariable::clearData);

    try {
        impl->subscription =
                std::unique_ptr<Impl::Subscription>(new Impl::Subscription(
                        impl.get(),
                        impl->pd_connection_.getProcess(),
                        impl->pd_connection_.getPathWithoutLocation()
                                .toStdString(),
                        impl->pd_connection_.getTransmission(),
                        getSelector(impl->pd_connection_)));
    }
    catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                               " \"%1\" with sample time %2: %3")
                               .arg(impl->pd_connection_.getPath())
                               .arg(impl->pd_connection_.getTransmission()
                                            .getInterval())
                               .arg(e.what());
        emit connectionChanged();
        return;
    }

    if (impl->pd_connection_.getTransmission().getInterval() == 0.0) {
        impl->pollOnce = true;
        // impl->subscription->poll(); // poll once to get initial value
    }
    emit connectionChanged();
}

/****************************************************************************/

QtPdCom::PdConnection PdVariable::getConnection()
{
    return impl->pd_connection_;
}

/****************************************************************************/

void PdVariable::setConnection(QtPdCom::PdConnection const &conn)
{
    impl->pd_connection_ = conn;
    updateConnection();
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void PdVariable::clearVariable()
{
    if (impl->subscription) {
        impl->subscription.reset();
        impl->dataPresent = false;
        emit dataPresentChanged(impl->dataPresent);

        clearData();
    }
}

/****************************************************************************/

/** Connected state.
 */
bool PdVariable::hasVariable() const
{
    return impl->subscription
            and not impl->subscription->getVariable().empty();
}

/****************************************************************************/

void PdVariable::clearData()
{
    if (impl->dataPresent) {
        if (impl->subscription) {
            impl->subscription->clearGenerator();
        }
        impl->dataPresent = false;
        emit dataPresentChanged(impl->dataPresent);

        emit valueChanged();
    }
}

/****************************************************************************/

QVariant PdVariable::getValue() const
{
    if (impl->subscription) {
        return impl->subscription->getValue();
    }
    return {};
}

/****************************************************************************/

void PdVariable::setValue(QVariant value)
{
    if (!impl->subscription or impl->subscription->getVariable().empty()
        or !impl->dataPresent) {
        return;
    }

    PdCom::Variable pv(impl->subscription->getVariable());
    auto nelem(pv.getSizeInfo().totalElements());
    int cnt = nelem;
    bool isNumber {false};
    value.toDouble(&isNumber);
    bool isList = value.canConvert<QVariantList>();  // see assistant about
                                                     // canConvert() and
                                                     // convert()

    // a string can be stored in an uint8 array as well
    if (value.canConvert<QString>() and not isNumber and not isList) {
        setValueAsString(value.toString());
        return;  // can't be anything else
    }

    if (!impl->subscription->setValue(value)) {
        qWarning() << "setting"
                   << QString::fromStdString(
                              impl->subscription->getVariable().getPath())
                   << "to" << value << "failed.";
    }
}

/****************************************************************************/
/** Increments the current #value and writes it to the process.
 *
 * This does \a not update #value directly.
 */
void PdVariable::inc()
{
    auto v = getValue();
    if (v.canConvert<double>()) {
        double d = v.toDouble() + 1;
        setValue(d);
    }
}
/****************************************************************************/

QString PdVariable::getValueAsString() const
{
    if (!impl->subscription || !impl->dataPresent) {
        return QString();
    }
    const auto var = impl->subscription->getVariable();
    Q_ASSERT(!var.empty());
    switch (var.getTypeInfo().type) {
        case PdCom::TypeInfo::char_T:
        case PdCom::TypeInfo::int8_T:
        case PdCom::TypeInfo::uint8_T:
            break;
        default:
            return QString();
    }
    const auto max_sz = var.getSizeInfo().totalElements();
    const auto str = static_cast<const char *>(impl->subscription->getData());
    const auto count = std::find(str, str + max_sz, '\0') - str;
    return QString::fromUtf8(str, count);
}

/****************************************************************************/

void PdVariable::setValueAsString(QString value)
{
    if (!impl->subscription or impl->subscription->getVariable().empty()
        or !impl->dataPresent) {
        return;
    }

    PdCom::Variable pv(impl->subscription->getVariable());
    auto nelem(pv.getSizeInfo().totalElements());
    int cnt = nelem;

    // a string can be stored in an uint8 array as well
    using dtype = std::remove_reference_t<
            decltype(*std::declval<QByteArray>().data())>;

    if (pv.getTypeInfo().element_size == sizeof(dtype)) {
        QString s = value;
        QByteArray data;
        do { /* the string with the trailing 0 must not be longer than the
                array.  We need to chop the string and not the byteArray
                because there can be an utf8 char at the end which is longer
                than one byte */
            data = s.toUtf8();
            s.chop(1);
        } while (data.size() > cnt - 1);
        data.append('\0');
        pv.setValue(
                data.data(),
                PdCom::details::TypeInfoTraits<dtype>::type_info.type,
                data.size());
    }
    else {
        qWarning() << "type error: string is supplied to PdVector "
                   << "but variable is not char/int8/uint8";
    }
}

/****************************************************************************/

std::chrono::nanoseconds PdVariable::getMTime() const
{
    return impl->mTime;
}

/****************************************************************************/

double PdVariable::getMTimeToDouble() const
{
    return std::chrono::duration<double>(impl->mTime).count();
}

/****************************************************************************/

bool PdVariable::getDataPresent()
{
    return impl->dataPresent;
}
