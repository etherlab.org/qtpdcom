/*****************************************************************************
 *
 * Copyright (C) 2018-2021  Wilhelm Hagemeister<hm@igh.de>
 *               2018-2022  Florian Pose <fp@igh.de>
 *               2018-2024  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QObject>
#include <QQmlEngine>
#include <QtPdCom1/Process.h>
#include <QtPdCom1/Transmission.h>
#include <pdcom5/Selector.h>

namespace QtPdCom {

class ScalarSubscriber;

class PdConnection
{
        Q_GADGET
        QML_VALUE_TYPE(pdConnection)
        QML_ADDED_IN_VERSION(1, 4)

        Q_PROPERTY(QtPdCom::Process *process READ getProcess WRITE setProcess)
        Q_PROPERTY(QString path READ getPath WRITE setPath)
        Q_PROPERTY(QVariant period READ getTransmissionVariant WRITE
                           setTransmission)

        Q_PROPERTY(double offset READ getOffset WRITE setOffset)
        Q_PROPERTY(double scale READ getScale WRITE setScale)
        Q_PROPERTY(double tau READ getTau WRITE setTau)

    public:
        PdConnection() = default;

        QtPdCom::Process *getProcess() const;
        QString getPath() const { return path_; }
        QVariant getTransmissionVariant() const;
        Transmission getTransmission() const { return transmission_; }
        double getOffset() const { return offset_; }
        double getScale() const { return scale_; }
        double getTau() const { return tau_; }

        PdCom::Selector getSelector(bool *ok = nullptr) const;
        QString getPathWithoutLocation() const;

        bool setVariable(
                ScalarSubscriber &subscriber,
                bool ignore_selector = false);

    private:
        void setProcess(QtPdCom::Process *);
        void setPath(QString);
        void setTransmission(QVariant);
        void setOffset(double);
        void setScale(double);
        void setTau(double);

        QtPdCom::Process *process_ = nullptr;
        QString path_;
        QtPdCom::Transmission transmission_ = QtPdCom::event_mode;
        double offset_ = 0.0;
        double scale_ = 1.0;
        double tau_ = 0.0;
};

}  // namespace QtPdCom
