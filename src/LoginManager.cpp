/*****************************************************************************
 *
 * Copyright (C) 2009-2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "LoginManager.h"

#include "LoginManager_p.h"
#include <pdcom5/Exception.h>
#include <QDebug>

using QtPdCom::LoginManager;
using QtPdCom::LoginManagerPrivate;

LoginManager::LoginManager(QString server_name, QObject *parent):
    QObject(parent),
    d_ptr(new LoginManagerPrivate(server_name.toStdString().c_str(), this))
{}

LoginManager::~LoginManager() = default;

void LoginManager::InitLibrary(const char *plugin_path)
{
    PdCom::SimpleLoginManager::InitLibrary(plugin_path);
}

void LoginManager::FinalizeLibrary()
{
    PdCom::SimpleLoginManager::FinalizeLibrary();
}

void LoginManager::setAuthName(QString name)
{
    Q_D(LoginManager);
    d->username = name.toStdString();
    d->username_set = true;
}

void LoginManager::setPassword(QString password)
{
    Q_D(LoginManager);
    d->password = password.toStdString();
    d->password_set = true;
}

void LoginManager::login()
{
    Q_D(LoginManager);
    try {
        d->login();
    }
    catch (const PdCom::NotConnected &) {
        qWarning() << "Login on not connected process!";
    }
}

void LoginManager::logout()
{
    Q_D(LoginManager);
    try {
        d->logout();
    }
    catch (const PdCom::NotConnected &) {
    }
}

std::string LoginManagerPrivate::getAuthname()
{
    if (username_set) {
        return username;
    }
    Q_Q(LoginManager);
    emit q->needCredentials();
    // username can be set from non-queued signal
    if (username_set) {
        return username;
    }
    throw Cancel();
}

std::string LoginManagerPrivate::getPassword()
{
    if (password_set) {
        return password;
    }
    Q_Q(LoginManager);
    emit q->needCredentials();
    // username can be set from non-queued signal
    if (password_set) {
        return password;
    }
    throw Cancel();
}

void LoginManagerPrivate::completed(
        PdCom::SimpleLoginManager::LoginResult result)
{
    Q_Q(LoginManager);
    loginResult = result;
    if (result == LoginResult::Success) {
        emit q->loginSuccessful();
    }
    else if (result == LoginResult::Error) {
        clearCredentials();
        emit q->loginFailed();
    }
    else if (result == LoginResult::NoSaslMechanism) {
        emit q->loginFailed();
    }
    // LoginResult::Canceled is not an error.
}

void LoginManagerPrivate::clearCredentials()
{
    username.clear();
    password.clear();
    password_set = username_set = false;
}

void LoginManager::clearCredentials()
{
    Q_D(LoginManager);
    d->clearCredentials();
}

QString LoginManager::getErrorMessage()
{
    Q_D(LoginManager);

    switch (d->loginResult) {
        case PdCom::SimpleLoginManager::LoginResult::Success:
            return tr("Login successful.");
        case PdCom::SimpleLoginManager::LoginResult::Error:
            return tr("Login failed: Please check"
                      " the given username and password.");
        case PdCom::SimpleLoginManager::LoginResult::Canceled:
            return tr("Login was aborted.");
        case PdCom::SimpleLoginManager::LoginResult::NoSaslMechanism:
            return tr("Login failed: No authentication mechanism found.");
        default:
            return tr("Unknown login error");
    }
}
