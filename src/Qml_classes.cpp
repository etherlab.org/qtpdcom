/*****************************************************************************
 *
 * Copyright (C) 2024 Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Qml_classes.h"

#include "QtPdCom1.h"

#include <QTimer>
#include <chrono>

#ifdef QTPDCOM_HAS_LOGIN_MANAGER
#include <QtPdCom1/LoginManager.h>
#endif

QtPdCom::SaslInitializer::SaslInitializer(QObject *parent):
    QObject(parent)
{
#ifdef QTPDCOM_HAS_LOGIN_MANAGER
    QtPdCom::LoginManager::InitLibrary();
#endif
}

QtPdCom::SaslInitializer::~SaslInitializer()
{
#ifdef QTPDCOM_HAS_LOGIN_MANAGER
    QtPdCom::LoginManager::FinalizeLibrary();
#endif
}


void QtPdCom::QmlProcess::ping(QJSValue jsCallback, double const timeout)
{
    const auto begin = std::chrono::steady_clock::now();
    try {
        auto &fw = QtPdCom::Process::ping(
                qjsEngine(this),
                [this, jsCallback, begin]() {
                    const auto diff = std::chrono::duration<double>(
                            std::chrono::steady_clock::now() - begin);
                    jsCallback.call(
                            {qjsEngine(this)->toScriptValue(true),
                             qjsEngine(this)->toScriptValue(diff.count())});
                });
        if (timeout > 0) {
            auto timer = new QTimer(qjsEngine(this));
            timer->setSingleShot(true);
            connect(&fw, &QFutureWatcherBase::finished, timer, &QTimer::stop);
            connect(&fw,
                    &QFutureWatcherBase::finished,
                    timer,
                    &QTimer::deleteLater);
            connect(&fw, &QFutureWatcherBase::canceled, timer, &QTimer::stop);
            connect(&fw,
                    &QFutureWatcherBase::canceled,
                    timer,
                    &QTimer::deleteLater);
            connect(&fw,
                    &QFutureWatcherBase::canceled,
                    qjsEngine(this),
                    [this, timer, jsCallback]() {
                        jsCallback.call(
                                {qjsEngine(this)->toScriptValue(false),
                                 QJSValue {}});
                        timer->deleteLater();
                    });
            connect(timer,
                    &QTimer::timeout,
                    qjsEngine(this),
                    [this, timer, jsCallback]() {
                        jsCallback.call(
                                {qjsEngine(this)->toScriptValue(false),
                                 QJSValue {}});
                        timer->deleteLater();
                    });
            timer->start(timeout * 1000);
        }
    }
    catch (const PdCom::Exception &) {
        QTimer::singleShot(0, qjsEngine(this), [this, jsCallback]() {
            jsCallback.call(
                    {qjsEngine(this)->toScriptValue(false), QJSValue {}});
        });
    }
}

using QtPdCom::DefaultProcess;

DefaultProcess *DefaultProcess::s_singletonInstance;
QJSEngine *DefaultProcess::s_engine;

DefaultProcess::DefaultProcess(QObject *parent):
    QtPdCom::QmlProcess::QmlProcess(parent)
{}

void DefaultProcess::createDefaultInstance(QObject *parent)
{
    s_singletonInstance = new DefaultProcess(parent);
    QtPdCom::Process::setDefaultProcess(DefaultProcess::s_singletonInstance);
}

DefaultProcess *DefaultProcess::create(QQmlEngine *, QJSEngine *engine)
{
    // The instance has to exist before it is used. We cannot replace it.
    Q_ASSERT(s_singletonInstance);

    // The engine has to have the same thread affinity as the singleton.
    Q_ASSERT(engine->thread() == s_singletonInstance->thread());

    // There can only be one engine accessing the singleton.
    if (s_engine) {
        Q_ASSERT(engine == s_engine);
    }
    else {
        s_engine = engine;
    }

    // Explicitly specify C++ ownership so that the engine doesn't delete
    // the instance.
    QJSEngine::setObjectOwnership(
            s_singletonInstance,
            QJSEngine::CppOwnership);
    return s_singletonInstance;
}

void QtPdCom::QmlMessage::setMessage(
        const QtPdCom::Message *message,
        QString lang)
{
    if (!message) {
        active_ = false;
        emit contentChanged();
        return;
    }
    time_ = message->getTime();
    type_ = message->getType();
    path_ = message->getPath();
    text_ = message->getText(lang);
    index_ = message->getIndex();
    active_ = message->isActive();
    description_ = message->getDescription(lang);
    emit contentChanged();
}

QtPdCom::QmlMessageModel::QmlMessageModel(QObject *parent):
    QtPdCom::MessageModel(parent)
{
    const auto l = QLocale().name().split('_');
    if (!l.isEmpty()) {
        locale_ = l.first();
    }
    QObject::connect(
            this,
            &QtPdCom::MessageModel::currentMessage,
            this,
            [this](const QtPdCom::Message *msg) {
                current_message_.setMessage(msg, locale_);
            });
}
