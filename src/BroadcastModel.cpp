/*****************************************************************************
 *
 * Copyright (C) 2009-2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "BroadcastModel.h"

#include <QDateTime>
#include <QtPdCom1/Process.h>

namespace QtPdCom {
class BroadcastModelPrivate
{
    friend class BroadcastModel;


    struct Broadcast
    {
        QString message, timestamp, user;
    };

    QList<Broadcast> broadcasts;
    QMetaObject::Connection processConnection;
    Process *process = nullptr;
};
}  // namespace QtPdCom

using QtPdCom::BroadcastModel;

BroadcastModel::BroadcastModel(QObject *parent):
    QAbstractTableModel(parent), d_ptr(new BroadcastModelPrivate())
{}

BroadcastModel::~BroadcastModel() = default;

int BroadcastModel::rowCount(const QModelIndex &) const
{
    const Q_D(BroadcastModel);

    return d->broadcasts.count();
}

int BroadcastModel::columnCount(const QModelIndex &) const
{
    return 3;
}

QVariant BroadcastModel::data(const QModelIndex &index, int role) const
{
    const Q_D(BroadcastModel);
    int col = index.column();

    if (index.row() < 0 || index.row() >= d->broadcasts.count())
        return QVariant();
    switch (role) {
        case Qt::DisplayRole:
        case DateStringRole:
            break;
        case MessageStringRole:
            col = 1;
            break;
        case UsernameRole:
            col = 2;
            break;
        default:
            return QVariant();
    }
    switch (col) {
        case 0:
            return d->broadcasts[index.row()].timestamp;
        case 1:
            return d->broadcasts[index.row()].message;
        case 2:
            return d->broadcasts[index.row()].user;
        default:
            return QVariant();
    }
}

QVariant BroadcastModel::headerData(
        int section,
        Qt::Orientation orientation,
        int role) const
{
    switch (role) {
        case Qt::DisplayRole:
            break;
        case DateStringRole:
            section = 0;
            break;
        case MessageStringRole:
            section = 1;
            break;
        case UsernameRole:
            section = 2;
            break;
        default:
            return QVariant();
    }
    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("Date");
            case 1:
                return tr("Message");
            case 2:
                return tr("User");
            default:
                break;
        }
    }
    return QVariant();
}

void BroadcastModel::connectProcess(QtPdCom::Process *p)
{
    Q_D(BroadcastModel);

    d->process = p;
    if (d->processConnection)
        disconnect(d->processConnection);

    if (!p)
        return;

    auto lambda = [this](const QString &message,
                         const QString & /* attr */,
                         uint64_t time_ns,
                         const QString &user) {
        Q_D(BroadcastModel);
        const auto sz = d->broadcasts.count();
        this->beginInsertRows({}, sz, sz);
        d->broadcasts.push_back(
                {message,
                 QDateTime::fromMSecsSinceEpoch(time_ns / 1000000).toString(),
                 user});
        this->endInsertRows();
    };

    d->processConnection =
            connect(p, &QtPdCom::Process::broadcastReceived, this, lambda);
}

QtPdCom::Process *BroadcastModel::getProcess() const
{
    const Q_D(BroadcastModel);
    return d->process;
}

void BroadcastModel::clear()
{
    Q_D(BroadcastModel);

    beginResetModel();
    d->broadcasts.clear();
    endResetModel();
}

QHash<int, QByteArray> BroadcastModel::roleNames() const
{
    // default role names
    auto roles = QAbstractTableModel::roleNames();

    // extended role names
    roles[DateStringRole] = "dateString";
    roles[MessageStringRole] = "messageString";
    roles[UsernameRole] = "username";
    return roles;
}
