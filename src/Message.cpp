/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Message.h"

#include "MessageImpl.h"
#include "MessageItem.h"

#include <QDateTime>

namespace QtPdCom {

/****************************************************************************/

/** Constructor.
 */
Message::Message(QObject *parent):
    QObject(parent),
    impl(std::unique_ptr<Impl>(new Message::Impl(this)))
{
    static const auto mt =
            qRegisterMetaType<const Message *>("const QtPdCom::Message*");
    Q_UNUSED(mt);
}

/****************************************************************************/

/** Destructor.
 */
Message::~Message()
{}

/****************************************************************************/

/** \return True, if the message is currently active.
 */
bool Message::isActive() const
{
    return impl->currentItem;
}

/****************************************************************************/

/** \return The message time in seconds.
 */
double Message::getTime() const
{
    if (impl->currentItem) {
        return impl->currentItem->setTime * 1e-9;
    }
    else {
        return 0.0;
    }
}

/****************************************************************************/

/** \return The message type.
 */
Message::Type Message::getType() const
{
    return impl->type;
}

/****************************************************************************/

/** \return The message path.
 */
const QString &Message::getPath() const
{
    return impl->path;
}

/****************************************************************************/

/** \return The message index.
 */
int Message::getIndex() const
{
    return impl->index;
}

/****************************************************************************/

/** \return The message text. If the text is not available in the desired
 * language, the default text (empty language) will be returned. If that is
 * also not available, display the message path.
 */
QString Message::getText(const QString &lang) const
{
    QString langText = impl->text.value(lang);
    if (not langText.isEmpty()) {
        return langText;
    }

    QString defaultLangText = impl->text.value("");
    if (not defaultLangText.isEmpty()) {
        return defaultLangText;
    }

    QString path = impl->path;
    if (impl->index > -1) {
        path += QString("#%1").arg(impl->index);
    }
    return path;
}

/****************************************************************************/

/** \return The message description.
 */
QString Message::getDescription(const QString &lang) const
{
    return impl->description.value(lang, impl->description.value(""));
}

/****************************************************************************/

/** Returns the message time as a string.
 */
QString Message::getTimeString() const
{
    quint64 t {impl->currentItem ? impl->currentItem->setTime : 0};
    return impl->timeString(t);
}

/****************************************************************************/

}  // namespace QtPdCom

/****************************************************************************/
