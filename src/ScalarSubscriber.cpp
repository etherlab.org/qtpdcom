/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ScalarSubscriber.h"
using QtPdCom::ScalarSubscriber;

#include <pdcom5/Process.h>
#include <pdcom5/Exception.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

#include <QStringList>
#include <QTimer>

/****************************************************************************/

class QtPdCom::ScalarSubscriber::Impl: public QObject
{
        Q_OBJECT

    public:
        Impl(ScalarSubscriber *parent):
            parent {parent},
            filterConstant {0.0},
            timer {nullptr}
        {}

        ScalarSubscriber *const parent;

        struct ScalarSubscription;
        std::unique_ptr<ScalarSubscription> subscription;

        double filterConstant; /**< PT1 filter constant. */

        QTimer *timer; /**< Timer for poll mode. */

        void createTimer(double);
        void deleteTimer();
        void startTimer();
        void stopTimer();

    private slots:
        void timeout();
};

/****************************************************************************/

struct QtPdCom::ScalarSubscriber::Impl::ScalarSubscription:
    public PdCom::Subscriber,
    public PdCom::Subscription
{
    public:
        ScalarSubscription(
                ScalarSubscriber::Impl *parent,
                PdCom::Variable pv,
                const PdCom::Selector &selector,
                const Transmission &transmission):
            Subscriber {transmission.toPdCom()},
            Subscription {*this, pv, selector},
            parent {parent},
            selector {selector}
        {
#ifdef DEBUG_PD_SCALARSUBSCRIBER
            qDebug() << this << __func__ << "var" << pv.getPath().c_str();
#endif
        }

        ScalarSubscription(
                ScalarSubscriber::Impl *parent,
                PdCom::Process *process,
                const std::string &path,
                const PdCom::Selector &selector,
                const Transmission &transmission):
            Subscriber {transmission.toPdCom()},
            Subscription {*this, *process, path, selector},
            parent {parent},
            selector {selector}
        {
#ifdef DEBUG_PD_SCALARSUBSCRIBER
            qDebug() << this << __func__ << "path" << path.c_str();
#endif
        }

        const PdCom::Selector &getSelector() const { return selector; }

    private:
        ScalarSubscriber::Impl *parent;
        const PdCom::Selector selector;

        void stateChanged(const PdCom::Subscription &) override
        {
#ifdef DEBUG_PD_SCALARSUBSCRIBER
            QString path;
            if (not getVariable().empty()) {
                path = getVariable().getPath().data();
            }
            qDebug() << this << __func__ << (int) getState() << path;
#endif

            if (getState() == PdCom::Subscription::State::Active) {
                // changed to active. If event mode, poll once.
                if (getTransmission() == PdCom::event_mode
                    or getTransmission() == PdCom::poll_mode) {
                    poll();  // poll once to get initial value
                }
            }

            if (getState() != PdCom::Subscription::State::Active) {
#ifdef DEBUG_PD_SCALARSUBSCRIBER
                qDebug() << this << "inactive";
#endif
                parent->stopTimer();
            }

            parent->parent->stateChange(getState());
        }

        void newValues(std::chrono::nanoseconds ts) override
        {
            parent->parent->newValues(ts);

            if (getTransmission() == PdCom::poll_mode) {
                parent->startTimer();
            }
        }
};

/*****************************************************************************
 * Implementation class.
 ****************************************************************************/

void QtPdCom::ScalarSubscriber::Impl::createTimer(double interval)
{
#ifdef DEBUG_PD_SCALARSUBSCRIBER
    qDebug() << this << __func__ << interval;
#endif

    if (timer) {
        return;
    }

    timer = new QTimer(this);
    timer->setSingleShot(true);
    timer->setInterval(interval * 1000.0);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeout()));
}

/****************************************************************************/

void QtPdCom::ScalarSubscriber::Impl::deleteTimer()
{
#ifdef DEBUG_PD_SCALARSUBSCRIBER
    qDebug() << this << __func__;
#endif

    if (not timer) {
        return;
    }

    delete timer;
    timer = nullptr;
}

/****************************************************************************/

void QtPdCom::ScalarSubscriber::Impl::startTimer()
{
#ifdef DEBUG_PD_SCALARSUBSCRIBER
    qDebug() << this << __func__;
#endif

    if (not timer or timer->interval() == 0) {
        // qWarning() << "No timer to start";
        return;
    }

    timer->start();
}

/****************************************************************************/

void QtPdCom::ScalarSubscriber::Impl::stopTimer()
{
#ifdef DEBUG_PD_SCALARSUBSCRIBER
    qDebug() << this << __func__;
#endif

    if (not timer) {
        // qWarning() << "No timer to stop";
        return;
    }

    timer->stop();
}

/****************************************************************************/

void QtPdCom::ScalarSubscriber::Impl::timeout()
{
#ifdef DEBUG_PD_SCALARSUBSCRIBER
    qDebug() << this << __func__;
#endif

    try {
        subscription->poll();
    }
    catch (std::exception &e) {
        qWarning() << "Failed to poll:" << e.what();
    }
}

/*****************************************************************************
 * Public class.
 ****************************************************************************/

/** Constructor.
 */
ScalarSubscriber::ScalarSubscriber():
    scale {1.0},
    offset {0.0},
    impl {std::unique_ptr<Impl> {new Impl {this}}}
{}

/****************************************************************************/

/** Destructor.
 */
ScalarSubscriber::~ScalarSubscriber()
{
    clearVariable();
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void ScalarSubscriber::setVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau)
{
    clearVariable();

    if (pv.empty()) {
        return;
    }

    this->scale = scale;
    this->offset = offset;

    if (tau > 0.0
        and (transmission.isContinuous() or transmission.isPoll())) {
        impl->filterConstant = transmission.getInterval() / tau;
    }
    else {
        impl->filterConstant = 0.0;
    }

    try {
        impl->subscription = std::unique_ptr<Impl::ScalarSubscription>(
                new Impl::ScalarSubscription(
                        impl.get(),
                        pv,
                        selector,
                        transmission));
    }
    catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                               " \"%1\" with transmission %2: %3")
                               .arg(QString(pv.getPath().c_str()))
                               .arg(transmission.toString())
                               .arg(e.what());
        return;
    }

    if (transmission.isPoll()) {
        impl->createTimer(transmission.getInterval());
    }
    pv.getProcess()->callPendingCallbacks();
}

/****************************************************************************/

/** Subscribes to a ProcessVariable.
 */
void ScalarSubscriber::setVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission,
        double scale,
        double offset,
        double tau)
{
    clearVariable();

    if (path.isEmpty() or not process) {
        return;
    }

    this->scale = scale;
    this->offset = offset;

    if (tau > 0.0
        and (transmission.isContinuous() or transmission.isPoll())) {
        impl->filterConstant = transmission.getInterval() / tau;
    }
    else {
        impl->filterConstant = 0.0;
    }

    try {
        impl->subscription = std::unique_ptr<Impl::ScalarSubscription>(
                new Impl::ScalarSubscription(
                        impl.get(),
                        process,
                        path.toStdString(),
                        selector,
                        transmission));
    }
    catch (PdCom::Exception &e) {
        qCritical() << QString("Failed to subscribe to variable"
                               " \"%1\" with sample time %2: %3")
                               .arg(path)
                               .arg(transmission.toString())
                               .arg(e.what());
        return;
    }

    if (transmission.isPoll() and not impl->timer) {
        impl->createTimer(transmission.getInterval());
    }
}

/****************************************************************************/

/** Unsubscribe from a Variable.
 */
void ScalarSubscriber::clearVariable()
{
#ifdef DEBUG_PD_SCALARSUBSCRIBER
    qDebug() << impl.get() << __func__;
#endif

    if (impl->subscription) {
        impl->subscription.reset();
        stateChange(PdCom::Subscription::State::Invalid);
    }

    impl->deleteTimer();
}

/****************************************************************************/

bool ScalarSubscriber::hasVariable() const
{
    return impl->subscription
            and not impl->subscription->getVariable().empty();
}

/****************************************************************************/

void ScalarSubscriber::stateChange(PdCom::Subscription::State)
{}

/****************************************************************************/

double ScalarSubscriber::getFilterConstant() const
{
    return impl->filterConstant;
}

/****************************************************************************/

PdCom::Variable ScalarSubscriber::getVariable() const
{
    if (impl->subscription) {
        return impl->subscription->getVariable();
    }
    else {
        return PdCom::Variable();
    }
}

/****************************************************************************/

const void *ScalarSubscriber::getData() const
{
    if (impl->subscription) {
        return impl->subscription->getData();
    }
    else {
        return nullptr;
    }
}

/****************************************************************************/

bool QtPdCom::ScalarSubscriber::poll()
{
    if (!impl->subscription
        || impl->subscription->getState()
                != PdCom::Subscription::State::Active) {
        return false;
    }
    impl->subscription->poll();
    return true;
}

/****************************************************************************/

const PdCom::Selector &ScalarSubscriber::getSelector() const
{
    return impl->subscription->getSelector();
}

/****************************************************************************/

// Tell qmake, that there are subclasses of QObject defined here and MOC must
// be run on this file.
#include "ScalarSubscriber.moc"

/****************************************************************************/
