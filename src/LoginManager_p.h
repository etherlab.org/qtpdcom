/*****************************************************************************
 *
 * Copyright (C) 2009-2023  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_LOGINMANAGERP_H
#define QTPDCOM_LOGINMANAGERP_H

#include <pdcom5/SimpleLoginManager.h>
#include <string>
#include <QtCore/QtGlobal>


namespace QtPdCom {

class LoginManager;

class LoginManagerPrivate: public PdCom::SimpleLoginManager
{
    std::string getAuthname() override;
    std::string getPassword() override;
    void completed(LoginResult success) override;
    void clearCredentials();

  public:
    LoginManagerPrivate(const char *server_name, LoginManager *q):
        PdCom::SimpleLoginManager(server_name), q_ptr(q)
    {}

    LoginManager *const q_ptr;
  private:
    Q_DECLARE_PUBLIC(LoginManager);
    std::string username, password;
    bool username_set = false, password_set = false;
    PdCom::SimpleLoginManager::LoginResult loginResult =
        PdCom::SimpleLoginManager::LoginResult::Success;
};

}  // namespace QtPdCom

#endif  // QTPDCOM_LOGINMANAGERP_H
