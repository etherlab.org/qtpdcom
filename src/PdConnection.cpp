/*****************************************************************************
 *
 * Copyright (C) 2018-2021  Wilhelm Hagemeister<hm@igh.de>
 *               2018-2022  Florian Pose <fp@igh.de>
 *               2018-2024  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "PdConnection.h"

#include <QtPdCom1/ScalarSubscriber.h>
#include "Qml_classes.h"

#include <QDebug>

using QtPdCom::PdConnection;

QtPdCom::Process *PdConnection::getProcess() const
{
    if (process_) {
        return process_;
    }
    return QtPdCom::DefaultProcess::s_singletonInstance;
}

void PdConnection::setProcess(QtPdCom::Process *process)
{
    process_ = process;
}

void PdConnection::setPath(QString path)
{
    path_ = path;
}

QVariant PdConnection::getTransmissionVariant() const
{
    if (transmission_ == QtPdCom::manual_poll_mode) {
        return QString("poll");
    }
    else {
        return transmission_.getInterval();
    }
}

static QtPdCom::Transmission getTrans(double sampleTime)
{
    if (sampleTime > 0.0) {
        return {std::chrono::duration<double>(sampleTime)};
    }
    if (sampleTime == 0.0) {
        return {QtPdCom::event_mode};
    }
    return {QtPdCom::poll_mode, -sampleTime};
}

void PdConnection::setTransmission(QVariant const t)
{
    QtPdCom::Transmission transmission = QtPdCom::manual_poll_mode;
    if (t.canConvert<double>()) {
        transmission = getTrans(t.toDouble());
    }
    else if (!t.canConvert<QString>() || t.toString() != QString("poll")) {
        qWarning() << "cannot convert " << t << " to Transmission.";
        return;
    }
    transmission_ = transmission;
}

void PdConnection::setOffset(double offset)
{
    offset_ = offset;
}

void PdConnection::setScale(double scale)
{
    scale_ = scale;
}

void PdConnection::setTau(double tau)
{
    tau_ = tau;
}

PdCom::Selector PdConnection::getSelector(bool *ok) const
{
    bool _ok;
    if (!ok) {
        ok = &_ok;
    }

    const QStringList pathElements = getPath().split(
            '#',
#if QT_VERSION > 0x050f00
            Qt::SkipEmptyParts
#else
            QString::SkipEmptyParts
#endif
    );

    if (pathElements.count() == 2) {
        int index = pathElements.at(1).toInt(ok);
        if (!*ok) {
            qCritical() << "Only integer as path selector allowed "
                           "currently! Not registering the variable "
                        << getPath();
            return {};
        }
        return PdCom::ScalarSelector {{index}};
    }
    *ok = pathElements.count() == 1;
    return {};
}

QString PdConnection::getPathWithoutLocation() const
{
    const QStringList pathElements = getPath().split(
            '#',
#if QT_VERSION > 0x050f00
            Qt::SkipEmptyParts
#else
            QString::SkipEmptyParts
#endif
    );
    return pathElements.first();
}

bool PdConnection::setVariable(
        ScalarSubscriber &subscriber,
        bool ignore_selector)
{
    if (!getProcess()) {
        return false;
    }
    bool ok = true;
    const auto selector =
            ignore_selector ? PdCom::Selector {} : getSelector(&ok);
    if (!ok) {
        return false;
    }
    subscriber.setVariable(
            getProcess(),
            getPathWithoutLocation(),
            selector,
            transmission_,
            scale_,
            offset_,
            tau_);
    return true;
}
