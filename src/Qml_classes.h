/*****************************************************************************
 *
 * Copyright (C) 2024 Bjarne von Horn <vh at igh dot de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QQmlEngine>
#include <QtPdCom1/ClientStatisticsModel.h>
#include <QtPdCom1/Process.h>
#include <QtPdCom1/LoginManager.h>
#include <QtPdCom1/MessageModel.h>
#include <QtPdCom1/MessageModelFilter.h>
#include <QTranslator>
#include <QJSValue>

namespace QtPdCom {


/** QML-Adapted Process Class.
 *
 * This class is specialized for QML applications.
 * A singleton is created for you, \c DefaultProcess .
 * Micro example (paste into a file and open it with \c qml6 ):
 * \code{.qml}
 * import QtQuick
 * import QtQuick.Controls
 * import de.igh.qtpdcom 1.4
 *
 * ApplicationWindow {
 *   width: 1024
 *   height: 600
 *   visible: true
 *
 *   Label {
 *      anchors.centerIn: parent
 *      text: "Server: " + DefaultProcess.name
 *   }
 *
 *   Component.onCompleted: DefaultProcess.connectToHost("localhost", 2345)
 * }
 * \endcode
 *
 */
class Q_DECL_EXPORT QmlProcess: public QtPdCom::Process
{
        Q_OBJECT
        QML_NAMED_ELEMENT(Process)
        QML_ADDED_IN_VERSION(1, 4)

    public:
        using QtPdCom::Process::Process;

        /** Ping server.
         *
         * \param jsCallback Callback with two arguments: success (bool). true
         *                   if ping was successful, false if not. duration
         * (double), elapsed time in seconds.
         *
         * \param timeout Timeout, in
         * seconds, if greater than zero.
         *
         * \code
         * Button {
         *      text: "ping"
         *      onClicked: DefaultProcess.ping((succes, duration) =>
         *                 console.log(success, duration), 1.0)
         * }
         * \endcode
         */

        Q_INVOKABLE void ping(QJSValue jsCallback, double timeout = 0);
};

class QmlMessage: public QObject
{
        Q_OBJECT
        QML_ANONYMOUS
        Q_PROPERTY(QString path READ getPath NOTIFY contentChanged)
        Q_PROPERTY(QString text READ getText NOTIFY contentChanged)
        Q_PROPERTY(
                QString description READ getDescription NOTIFY contentChanged)
        Q_PROPERTY(bool active READ isActive NOTIFY contentChanged)
        Q_PROPERTY(double time READ getTime NOTIFY contentChanged)
        Q_PROPERTY(int index READ getIndex NOTIFY contentChanged)
        Q_PROPERTY(int type READ getType NOTIFY contentChanged)


    public:
        using QObject::QObject;

        void setMessage(const QtPdCom::Message *message, QString lang);

        QString getPath() const { return path_; }
        QString getText() const { return text_; }
        QString getDescription() const { return description_; }
        double getTime() const { return time_; }
        int getType() const { return type_; }
        int getIndex() const { return index_; }
        bool isActive() const { return active_; }

    signals:
        void contentChanged();

    private:
        QString path_, text_, description_;
        double time_ = 0.0;
        int type_ = 0;
        int index_ = -1;
        bool active_ = false;
};

class QmlMessageModel: public MessageModel
{
        Q_OBJECT
        QML_NAMED_ELEMENT(MessageModel)
        QML_ADDED_IN_VERSION(1, 4)

        Q_PROPERTY(QString locale READ getLocale WRITE setLocale NOTIFY
                           localeChanged)

        Q_PROPERTY(QmlMessage *currentMessage READ getCurrentMessage()
                           CONSTANT)

    public:
        QmlMessageModel(QObject *parent = nullptr);


        void setLocale(QString locale)
        {
            locale_ = locale;
            emit localeChanged();
        }
        QString getLocale() const { return locale_; }
        QmlMessage *getCurrentMessage() { return &current_message_; }

    signals:
        void localeChanged();

    private:
        QString locale_;
        QmlMessage current_message_;
};

struct PdComMessageModelFilter
{
        Q_GADGET
        QML_FOREIGN(QtPdCom::MessageModelFilter)
        QML_NAMED_ELEMENT(MessageModelFilter)
        QML_ADDED_IN_VERSION(1, 4)
};

struct PdClientStatisticsModel
{
        Q_GADGET
        QML_FOREIGN(QtPdCom::ClientStatisticsModel)
        QML_NAMED_ELEMENT(ClientStatisticsModel)
        QML_ADDED_IN_VERSION(1, 4)
};

struct PdComLoginManager
{
        Q_GADGET
        QML_FOREIGN(QtPdCom::LoginManager)
        QML_NAMED_ELEMENT(LoginManager)
        QML_ADDED_IN_VERSION(1, 4)
};

class Q_DECL_EXPORT SaslInitializer: public QObject
{
        Q_OBJECT
        QML_ELEMENT
        QML_ADDED_IN_VERSION(1, 4)

    public:
        explicit SaslInitializer(QObject *parent = nullptr);
        ~SaslInitializer();
};

class Q_DECL_EXPORT DefaultProcess: public QtPdCom::QmlProcess
{
        Q_OBJECT
        QML_SINGLETON
        QML_NAMED_ELEMENT(DefaultProcess)
        QML_ADDED_IN_VERSION(1, 4)

    public:
        static DefaultProcess *s_singletonInstance;

        static DefaultProcess *create(QQmlEngine *, QJSEngine *engine);

        static void createDefaultInstance(QObject *parent = nullptr);

    private:
        static QJSEngine *s_engine;
        explicit DefaultProcess(QObject *parent);
};
}  // namespace QtPdCom
