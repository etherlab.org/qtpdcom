/*****************************************************************************
 *
 * Copyright (C) 2024  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#pragma once

#include <QtQml/qqmlextensionplugin.h>
#include <QtPdCom1/Process.h>
#include <QQmlEngine>

extern void Q_DECL_EXPORT qml_register_types_de_igh_qtpdcom();
Q_GHS_KEEP_REFERENCE(qml_register_types_de_igh_qtpdcom())

class Q_DECL_EXPORT de_igh_qtpdcomPlugin: public QQmlEngineExtensionPlugin
{
        Q_OBJECT
        Q_PLUGIN_METADATA(IID QQmlEngineExtensionInterface_iid)

    public:
        de_igh_qtpdcomPlugin(QObject *parent = nullptr);

        void initializeEngine(QQmlEngine *engine, const char *uri) override;
};
