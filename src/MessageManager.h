/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef QTPDCOM_MESSAGE_MANAGER_H
#define QTPDCOM_MESSAGE_MANAGER_H

#include <pdcom5/MessageManagerBase.h>
#include "FutureWatchers.h"

#include <QObject>
#include <QFuture>
#include <QFutureInterface>
#include <QFutureWatcher>
#include <QQueue>

/****************************************************************************/

namespace PdCom {
    class Process;
}

namespace QtPdCom {

using MessageFuture = QFuture<PdCom::Message>;
using MessageFutureInterface = QFutureInterface<PdCom::Message>;
using MessageListFuture = QFuture<std::vector<PdCom::Message>>;
using MessageListFutureInterface = QFutureInterface<std::vector<PdCom::Message>>;


class MessageWatcher : public QFutureWatcher<PdCom::Message>
{
    Q_OBJECT

    public:

        using QFutureWatcher<PdCom::Message>::QFutureWatcher;
};

class MessageListWatcher : public QFutureWatcher<std::vector<PdCom::Message>>
{
    Q_OBJECT

    public:

        using QFutureWatcher<std::vector<PdCom::Message>>::QFutureWatcher;
};

class MessageManager:
    public QObject,
    public PdCom::MessageManagerBase
{
    Q_OBJECT

    public:
        MessageManager();
        virtual ~MessageManager();

        void reset();

        MessageListFuture activeMessagesQt();
        MessageFuture getMessageQt(uint32_t seqNo);

        template <class Object, typename Callback>
        void activeMessages(Object *obj, Callback &&callback)
        {
            createWatcher<std::vector<PdCom::Message>>(obj, callback)
                .setFuture(activeMessagesQt());
        }

        template <class Object, typename Callback>
        void getMessage(uint32_t seqNo, Object *obj, Callback &&callback)
        {
            createWatcher<PdCom::Message>(obj, callback)
                .setFuture(getMessageQt(seqNo));
        }


    signals:
        void processMessageSignal(PdCom::Message message);
        void processResetSignal();

    private:
        void processMessage(PdCom::Message message) override;
        void getMessageReply(PdCom::Message message) override;
        void activeMessagesReply(std::vector<PdCom::Message> messageList) override;

        QQueue<MessageFutureInterface> getMessageQueue;
        QQueue<MessageListFutureInterface> activeMessageQueue;
};

}

#endif

/****************************************************************************/
