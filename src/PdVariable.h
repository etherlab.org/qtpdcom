/*****************************************************************************
 *
 * Copyright (C) 2018  Wilhelm Hagemeister <hm@igh.de>
 *                     Florian Pose <fp@igh.de>
 *               2024  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the License,
 * or (at your option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef PD_PdVariable_H
#define PD_PdVariable_H

#include <QtPdCom1/Process.h>

#include "PdConnection.h"

#include <QObject>
#include <QQmlEngine>
#include <QVariant>

#include <chrono>

namespace QtPdCom {

/****************************************************************************/

/** Scalar/Vector/Matrix Variant to be used in QML applications.
 */
class Q_DECL_EXPORT PdVariable: public QObject
{
        Q_OBJECT
        QML_NAMED_ELEMENT(PdVariable)
        QML_ADDED_IN_VERSION(1, 4)

        /** Connection property which describes the variable to connect to.
           @code{.json}
            connection {
                process: pdProcess // process instance, if null: use default.
                path: "/osc/cos"   // path (string)
                period: 0.2        // sample time, 0 for event,
                                   // "poll" for manual poll mode,
                                   // negative values for timer-basedpolling
                offset: 0.0        // offset (double), default 0
                scale: 1.0,        // scale (double), default 1.0
                tau:  0.0          // Low Pass Filter time constant
                                   // (double, in seconds). if 0: no filter
            }
            @endcode

         * The default Process instance is accessible via the DefaultProcess
         * singleton which is instantiated when importing the QtPdCom
         * library.
         */
        Q_PROPERTY(QtPdCom::PdConnection connection READ getConnection WRITE
                           setConnection NOTIFY connectionChanged)

        /** Indicates that the process is connected and data is transfered.
         */
        Q_PROPERTY(
                bool connected READ getDataPresent NOTIFY dataPresentChanged)

        /** Process value.
         */
        Q_PROPERTY(QVariant value READ getValue WRITE setValue NOTIFY
                           valueChanged)

        /** Interpret the elements of an process variable array as string.
         */
        Q_PROPERTY(QString text READ getValueAsString WRITE setValueAsString
                           NOTIFY valueChanged)

        /** last modification time of process variable
         */
        Q_PROPERTY(QVariant mtime READ getMTimeToDouble NOTIFY valueUpdated)

    public:
        PdVariable(QObject * = Q_NULLPTR);
        ~PdVariable();

        void clearVariable();
        bool hasVariable() const;
        void clearData();

        QVariant getValue() const;
        Q_INVOKABLE void setValue(QVariant);
        QString getValueAsString() const;
        Q_INVOKABLE void setValueAsString(QString);
        bool getDataPresent();

        std::chrono::nanoseconds getMTime() const;
        double getMTimeToDouble() const;

        PdConnection getConnection();
        void setConnection(PdConnection const &);
        void updateConnection(); /**< (re)connects to variable */

        Q_INVOKABLE void inc();

    signals:
        void valueChanged(); /**< Emitted, when the value changes, or the
                               variable is disconnected. */
        void valueUpdated(std::chrono::nanoseconds mtime); /**< Emitted also
                                                             when value does
                                                             not change but
                                                             got an update
                                                             from the msr
                                                             process */
        void pathChanged(QString);
        void connectionChanged();
        void dataPresentChanged(bool);

    private:
        class Q_DECL_HIDDEN Impl;
        std::unique_ptr<Impl> impl;
};

/****************************************************************************/

}  // namespace QtPdCom


#endif
