/*****************************************************************************
 *
 * Copyright (C) 2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MessageManager.h"

#include <QDebug>

using QtPdCom::MessageManager;

/****************************************************************************/

MessageManager::MessageManager()
{
}

/****************************************************************************/

MessageManager::~MessageManager()
{
    reset();
}

/****************************************************************************/

void MessageManager::reset()
{
    emit processResetSignal();
    while (!activeMessageQueue.empty())
        activeMessageQueue.dequeue().reportCanceled();
    while (!getMessageQueue.empty())
        getMessageQueue.dequeue().reportCanceled();
}

/****************************************************************************/

void MessageManager::processMessage(PdCom::Message message)
{
    emit processMessageSignal(message);
}

/****************************************************************************/

QtPdCom::MessageFuture MessageManager::getMessageQt(uint32_t seqNo)
{
    MessageFutureInterface promise;
    getMessageQueue.push_back(promise);
    promise.reportStarted();
    PdCom::MessageManagerBase::getMessage(seqNo);
    return promise.future();
}

/****************************************************************************/

void MessageManager::getMessageReply(PdCom::Message message)
{
    if (getMessageQueue.empty())
        return;

    auto promise = getMessageQueue.dequeue();
    if (promise.isCanceled())
        return;
    promise.reportResult(message);
    promise.reportFinished();
}

/****************************************************************************/

QtPdCom::MessageListFuture MessageManager::activeMessagesQt()
{
    MessageListFutureInterface promise;
    activeMessageQueue.push_back(promise);
    promise.reportStarted();
    PdCom::MessageManagerBase::activeMessages();
    return promise.future();
}

/****************************************************************************/

void MessageManager::activeMessagesReply(
        std::vector<PdCom::Message> messageList)
{
    if (activeMessageQueue.empty())
        return;

    auto promise = activeMessageQueue.dequeue();
    if (promise.isCanceled())
        return;
    promise.reportResult(messageList);
    promise.reportFinished();
}

/****************************************************************************/
