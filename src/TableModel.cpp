/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *                    2013  Dr. Wilhelm Hagemeister <hm@igh-essen.com>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "TableModel.h"
#include "TableModelImpl.h"

using QtPdCom::TableModel;

#include <QDebug>

/****************************************************************************/

/** Constructor.
 */
TableModel::TableModel(QObject *parent):
    QAbstractTableModel(parent),
    impl(new TableModel::Impl(this))
{
    connect(&impl->valueHighlightRow,
            SIGNAL(valueChanged()),
            this,
            SLOT(highlightRowChanged()));
    connect(&impl->visibleRowCount,
            SIGNAL(valueChanged()),
            this,
            SLOT(visibleRowCountChanged()));
}

/****************************************************************************/

/** Destructor.
 */
TableModel::~TableModel()
{
    impl->valueHighlightRow.clearVariable();
    clearColumns();
}

/****************************************************************************/

void TableModel::insertColumn(TableColumn *col, int position)
{
    if (position < 0 || position > impl->columnVector.count()) {
        position = impl->columnVector.count();
    }

    beginInsertColumns(QModelIndex(), position, position);
    impl->columnVector.insert(position, col);
    endInsertColumns();

    QObject::connect(
            col,
            SIGNAL(dimensionChanged()),
            this,
            SLOT(dimensionChanged()));
    QObject::connect(
            col,
            SIGNAL(headerChanged()),
            this,
            SLOT(columnHeaderChanged()));
    QObject::connect(col, SIGNAL(valueChanged()), this, SLOT(valueChanged()));

    impl->updateRows();
}

/****************************************************************************/

/** Adds a column.
 */
void TableModel::addColumn(TableColumn *column)
{
    insertColumn(column, -1);
}

/****************************************************************************/

void QtPdCom::TableModel::removeColumn(TableColumn *column)
{
    const auto idx = impl->columnVector.indexOf(column);
    if (idx == -1) {
        return;
    }

    beginRemoveColumns(QModelIndex(), idx, idx);
    impl->columnVector.remove(idx);
    endRemoveColumns();

    QObject::disconnect(
            column,
            SIGNAL(dimensionChanged()),
            this,
            SLOT(dimensionChanged()));
    QObject::disconnect(
            column,
            SIGNAL(headerChanged()),
            this,
            SLOT(columnHeaderChanged()));
    QObject::disconnect(
            column,
            SIGNAL(valueChanged()),
            this,
            SLOT(valueChanged()));

    impl->updateRows();
}

/****************************************************************************/

/** Clears the Columns.
 */
void TableModel::clearColumns()
{
    beginRemoveColumns(QModelIndex(), 0, impl->columnVector.count() - 1);
    impl->columnVector.clear();
    endRemoveColumns();

    for (const auto column : impl->columnVector) {
        QObject::disconnect(
                column,
                SIGNAL(dimensionChanged()),
                this,
                SLOT(dimensionChanged()));
        QObject::disconnect(
                column,
                SIGNAL(headerChanged()),
                this,
                SLOT(columnHeaderChanged()));
        QObject::disconnect(
                column,
                SIGNAL(valueChanged()),
                this,
                SLOT(valueChanged()));
    }

    impl->updateRows();
}

/****************************************************************************/

bool TableModel::isEditing() const
{
    bool editing = false;

    for (const auto column : impl->columnVector) {
        if (column->isEditing()) {
            editing = true;
            break;
        }
    }

    return editing;
}

/****************************************************************************/

unsigned int TableModel::getRowCapacity() const
{
    return impl->rowCapacity;
}

/****************************************************************************/

bool TableModel::hasVisibleRowsVariable() const
{
    return impl->visibleRowCount.hasVariable();
}

/****************************************************************************/

/** Implements the model interface.
 *
 * \returns Number of rows.
 */
int TableModel::rowCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return impl->rows;
    }
    else {
        return 0;
    }
}

/****************************************************************************/

/** Implements the model interface.
 *
 * \returns Number of columns.
 */
int TableModel::columnCount(const QModelIndex &index) const
{
    if (!index.isValid()) {
        return impl->columnVector.count();
    }
    else {
        return 0;
    }
}

/****************************************************************************/

/** Implements the Model interface.
 */
QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    return impl->columnVector[index.column()]->data(index.row(), role);
}

/****************************************************************************/

/** Implements the Model interface.
 */
QVariant
TableModel::headerData(int section, Qt::Orientation o, int role) const
{
    if (o != Qt::Horizontal) {
        return QVariant();
    }

    return impl->columnVector[section]->headerData(role);
}

/****************************************************************************/

/** Implements the Model interface.
 */
Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags f;

    if (index.isValid()) {
        f = impl->columnVector[index.column()]->flags(index.row());
        f |= Qt::ItemIsSelectable;
    }

    return f;
}

/****************************************************************************/

bool TableModel::setData(
        const QModelIndex &index,
        const QVariant &value,
        int role)
{
    if (!index.isValid()) {
        return false;
    }

    bool ret = impl->columnVector[index.column()]->setData(
            index.row(),
            value.toString(),
            role);

    emit editingChanged(isEditing());

    return ret;
}

/****************************************************************************/

bool TableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (row < 0 || count <= 0 || parent.isValid()) {
        return false;
    }

    if (!hasVisibleRowsVariable()) {
        return false;
    }

    if (row + count > impl->visibleRows) {
        return false;
    }

    impl->ensureEditing();

    beginRemoveRows(parent, row, row + count - 1);
    for (const auto column : impl->columnVector) {
        column->impl->deleteRow(row, count);
    }
    endRemoveRows();
    impl->visibleRows -= count;
    impl->updateRows();
    return true;
}

/****************************************************************************/

void TableModel::setHighlightRowVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission)
{
    clearHighlightRowVariable();

    if (pv.empty()) {
        return;
    }

    impl->valueHighlightRow.setVariable(pv, selector, transmission);
}

/****************************************************************************/

void TableModel::setHighlightRowVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission)
{
    clearHighlightRowVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->valueHighlightRow
            .setVariable(process, path, selector, transmission);
}

/****************************************************************************/

void TableModel::clearHighlightRowVariable()
{
    impl->valueHighlightRow.clearVariable();

    for (const auto column : impl->columnVector) {
        column->setHighlightRow(-1);
    }
}

/****************************************************************************/

int TableModel::fromCsv(
        const QString csvData,
        QChar separator,
        int startRow,
        int startColumn,
        const QLocale &locale)
{
#if QT_VERSION >= QT_VERSION_CHECK(5, 14, 0)
    const auto lines = csvData.split(QChar('\n'), Qt::SkipEmptyParts);
#else
    const auto lines = csvData.split(QChar('\n'), QString::SkipEmptyParts);
#endif
    if (startRow < 0 || startRow >= lines.size() || startColumn < 0) {
        return -ERANGE;
    }
    int row = 0;
    const bool wasEditing = isEditing();

    for (; row < (lines.size() - startRow); ++row) {
        const auto line = lines[row + startRow].split(separator);
        if (startColumn >= line.size()) {
            return -ERANGE;
        }
        const auto columnEnd = std::min<int>(
                impl->columnVector.size(),
                line.size() - startColumn);
        for (int column = 0; column < columnEnd; ++column) {
            impl->columnVector[column]->impl->setRow(
                    line[column + startColumn],
                    row,
                    locale);
        }
    }
    if (!wasEditing && isEditing()) {
        emit editingChanged(true);
    }

    impl->visibleRows =
            std::min<int>(row, impl->visibleRows + impl->rowCapacity);
    impl->updateRows();

    emit dataChanged(
            index(0, 0),
            index(rowCount({}) - 1, impl->columnVector.size() - 1));
    return row;
}

/****************************************************************************/

void TableModel::setVisibleRowsVariable(
        PdCom::Variable pv,
        const PdCom::Selector &selector,
        const Transmission &transmission)
{
    clearVisibleRowsVariable();

    if (pv.empty()) {
        return;
    }

    impl->visibleRowCount.setVariable(pv, selector, transmission);
}

/****************************************************************************/

void TableModel::setVisibleRowsVariable(
        PdCom::Process *process,
        const QString &path,
        const PdCom::Selector &selector,
        const Transmission &transmission)
{
    clearVisibleRowsVariable();

    if (not process or path.isEmpty()) {
        return;
    }

    impl->visibleRowCount.setVariable(process, path, selector, transmission);
}

/****************************************************************************/

void TableModel::setHighlightColor(QColor hc, int idx)
{
    if (idx <= -1) {
        for (const auto column : impl->columnVector) {
            column->setHighlightColor(hc);
        }
    }
    else if (idx < impl->columnVector.size()) {
        impl->columnVector[idx]->setHighlightColor(hc);
    }
}

/****************************************************************************/

void TableModel::setDisabledColor(QColor dc, int idx)
{
    if (idx <= -1) {
        for (const auto column : impl->columnVector) {
            column->setDisabledColor(dc);
        }
    }
    else if (idx < impl->columnVector.size()) {
        impl->columnVector[idx]->setDisabledColor(dc);
    }
}

/****************************************************************************/

void TableModel::clearVisibleRowsVariable()
{
    impl->visibleRowCount.clearVariable();
    impl->visibleRows = UINT_MAX;
    impl->updateRows();
}

/****************************************************************************/

/** Commits all edited data.
 */
void TableModel::commit()
{
    for (const auto column : impl->columnVector) {
        column->commit();
    }

    if (impl->visibleRowCount.hasVariable()
        && impl->visibleRowCount.getVariable().isWriteable()) {
        impl->visibleRowCount.writeValue(impl->visibleRows);
    }

    emit editingChanged(false);
}

/****************************************************************************/

/** Reverts all edited data.
 */
void TableModel::revert()
{
    for (const auto column : impl->columnVector) {
        column->revert();
    }

    if (impl->visibleRowCount.hasData()) {
        impl->visibleRows = impl->visibleRowCount.getValue();
    }
    else {
        impl->visibleRows = UINT_MAX;
    }
    impl->updateRows();

    emit editingChanged(false);
}

/****************************************************************************/

/** updates the visibleRowCount variable.

 */
void TableModel::addRow()
{
    if (impl->rowCapacity > 0) {
        ++impl->visibleRows;
        impl->updateRows();

        impl->ensureEditing();
    }
}

/** updates the visibleRowCount variable with initialization.
    if value is a scalar all columns will be initialized with the same value
    otherwise must be a QVariantList
 */
void TableModel::addRowAndCopyLast()
{
    if (impl->rowCapacity <= 0) {
        return;
    }

    if (impl->visibleRows <= 0) {
        // no row available, so nothing to copy
        addRow();
        return;
    }

    const bool wasEditing = isEditing();

    /* and initialize with prev row data */
    for (const auto &column : impl->columnVector) {
        const auto columnRows = column->getRows();
        if (columnRows == 0 || impl->visibleRows >= columnRows) {
            continue;
        }
        const auto v = column->data(impl->visibleRows - 1, Qt::DisplayRole)
                               .toString();
        column->setData(impl->visibleRows, v, Qt::EditRole);
    }
    ++impl->visibleRows;
    impl->updateRows();

    if (!wasEditing) {
        emit dataChanged(
                index(0, 0),
                index(impl->visibleRows - 1, impl->columnVector.count() - 1),
                {Qt::BackgroundRole});

        emit editingChanged(true);
    }
}

/****************************************************************************/

/** updates the visibleRowCount variable.

 */
bool TableModel::insertRows(
        int position,
        int count,
        const QModelIndex &parent)
{
    if (impl->rowCapacity < count || impl->columnVector.empty()) {
        return false;
    }
    if (parent.isValid() || !hasVisibleRowsVariable()) {
        return false;
    }
    const bool wasEditing = isEditing();
    for (const auto column : impl->columnVector) {
        column->impl->insertRow(position, count);
    }

    QModelIndex topLeft = index(position, 0);
    QModelIndex bottomRight =
            index(impl->visibleRows - 1, impl->columnVector.count() - 1);
    emit dataChanged(topLeft, bottomRight);
    impl->visibleRowCount.writeValue(impl->visibleRows + 1);

    if (!wasEditing) {
        emit dataChanged(
                index(0, 0),
                index(impl->visibleRows - 1, impl->columnVector.count() - 1),
                {Qt::BackgroundRole});

        emit editingChanged(true);
    }
    return true;
}

/****************************************************************************/

void TableModel::remRow()
{
    removeRows(impl->visibleRows - 1, 1, {});
}

/****************************************************************************/

/** Calculates the number of table rows.
 */
void TableModel::Impl::updateRows()
{
    ColumnVector::const_iterator it;
    unsigned int maxRows = 0;

    for (it = columnVector.begin(); it != columnVector.end(); it++) {
        unsigned int r = (*it)->getRows();
        if (r > maxRows) {
            maxRows = r;
        }
    }

    if (maxRows > visibleRows) {
        rowCapacity = maxRows - visibleRows;
        maxRows = visibleRows;
    }
    else {
        rowCapacity = 0;
    }

    if (maxRows > rows) {
        parent->beginInsertRows(QModelIndex(), rows, maxRows - 1);
        rows = maxRows;
        parent->endInsertRows();
    }
    else if (maxRows < rows) {
        parent->beginRemoveRows(QModelIndex(), maxRows, rows - 1);
        rows = maxRows;
        parent->endRemoveRows();
    }
}

/****************************************************************************/

void QtPdCom::TableModel::Impl::ensureEditing()
{
    if (parent->isEditing()) {
        return;
    }

    for (const auto column : columnVector) {
        column->impl->ensureEditData();
    }

    emit parent->dataChanged(
            parent->index(0, 0),
            parent->index(visibleRows - 1, columnVector.count() - 1),
            {Qt::BackgroundRole});

    emit parent->editingChanged(true);
}

/****************************************************************************/

/** Reacts on process variable dimension changes.
 */
void TableModel::dimensionChanged()
{
    impl->updateRows();
}

/****************************************************************************/

/** Reacts on header data changes.
 */
void TableModel::columnHeaderChanged()
{
    TableColumn *col = dynamic_cast<TableColumn *>(sender());
    int j = impl->columnVector.indexOf(col);

    if (j > -1) {
        headerDataChanged(Qt::Horizontal, j, j);
    }
}

/****************************************************************************/

/** Reacts on process variable changes.
 */
void TableModel::valueChanged()
{
    TableColumn *col = dynamic_cast<TableColumn *>(sender());

    int j = impl->columnVector.indexOf(col);
    if (j > -1) {
        QModelIndex topLeft = index(0, j);
        QModelIndex bottomRight =
                index(qMin(col->getRows(), impl->rows) - 1, j);
        emit dataChanged(topLeft, bottomRight);
        /* qDebug() << "Table changes: " << topLeft.row()
         * << topLeft.column() << bottomRight.row()
         * << bottomRight.column(); */
    }
}

/****************************************************************************/

void TableModel::highlightRowChanged()
{
    unsigned int row = -1;

    if (impl->valueHighlightRow.hasData()) {
        row = impl->valueHighlightRow.getValue();
    }

    for (const auto column : impl->columnVector) {
        column->setHighlightRow(row);
    }

    if ((impl->columnVector.count() > 0) && (row < impl->rows)) {
        QModelIndex topLeft = index(row, 0);
        QModelIndex bottomRight = index(row, impl->columnVector.count() - 1);
        emit dataChanged(topLeft, bottomRight);
    }
}

/****************************************************************************/

void TableModel::visibleRowCountChanged()
{
    if (impl->visibleRowCount.hasData() && !isEditing()) {
        impl->visibleRows = impl->visibleRowCount.getValue();
        impl->updateRows();
    }
}

/****************************************************************************/

QHash<int, QByteArray> TableModel::roleNames() const
{
    // default role names
    auto roles = QAbstractTableModel::roleNames();

    // extended role names
    roles[TableColumn::HighlightRole] = "highlight";
    roles[TableColumn::ValidRole] = "valid";
    roles[TableColumn::IsEnabledRole] = "isEnabled";
    roles[TableColumn::IsEditingRole] = "isEditing";
    roles[TableColumn::DecimalsRole] = "decimals";
    roles[TableColumn::LowerLimitRole] = "lowerLimit";
    roles[TableColumn::UpperLimitRole] = "upperLimit";
    return roles;
}

/****************************************************************************/

QString TableModel::toCsv(
        bool includeHeader,
        QChar seperator,
        const QLocale &locale) const
{
    QString ans;

    if (impl->columnVector.empty()) {
        return "";
    }

    const auto sanitziedHeader = [this](int idx) {
        QString ans = impl->columnVector[idx]->getHeader();
        ans.replace('\n', ' ');
        ans.replace('\r', QString());
        return ans;
    };

    if (includeHeader) {
        ans += sanitziedHeader(0);
        for (int column = 1; column < impl->columnVector.count(); ++column) {
            ans += seperator + sanitziedHeader(column);
        }
        ans += '\n';
    }

    for (int row = 0; row < rowCount({}); ++row) {
        ans += impl->columnVector[0]->impl->getRow(row, locale);
        for (int column = 1; column < impl->columnVector.count(); ++column) {
            ans += seperator
                    + impl->columnVector[column]->impl->getRow(row, locale);
        }
        ans += '\n';
    }
    return ans;
}

/****************************************************************************/
