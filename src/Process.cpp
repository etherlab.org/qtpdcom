/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "Process.h"
#include "MessageManager.h"

#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Exception.h>

#ifdef QTPDCOM_HAS_LOGIN_MANAGER
#include "LoginManager_p.h"
#include "LoginManager.h"
#endif


#ifdef __WIN32__
#include <windows.h>  // GetUserName(), gethostname()
#include <lmcons.h>   // UNLEN
#else
#include <unistd.h>  // getlogin()
#endif

#include <string>
#include <list>

#include <QTranslator>
#include <QFutureInterface>
#include <QQueue>
#include <QSslSocket>
#include <QSslKey>
#include <QSslCertificate>
#include <QSslConfiguration>

#include <QtPdCom1.h>
#include <git_revision_hash.h>

#define STR(x) #x
#define QUOTE(x) STR(x)

const char *const QtPdCom::qtpdcom_version_code = QUOTE(
        QTPDCOM_MAJOR) "." QUOTE(QTPDCOM_MINOR) "." QUOTE(QTPDCOM_RELEASE);

const char *const QtPdCom::qtpdcom_full_version = GIT_REV;

#define DEBUG_DATA 0

using QtPdCom::Process;
using VariablePromise = QFutureInterface<PdCom::Variable>;
using ListPromise = QFutureInterface<QtPdCom::VariableList>;
using PingPromise = QFutureInterface<void>;
using ClientStatisticsPromise =
        QFutureInterface<std::vector<PdCom::ClientStatistics>>;

/****************************************************************************/

struct Process::Impl
{
        Impl(Process *process):
            process(process),
            messageManager(),
            appName("QtPdCom1"),
            socket(),
            defaultSslConfig(socket.sslConfiguration()),
            socketValid(false),
            connectionState(Disconnected),
            rxBytes(0),
            txBytes(0)
        {
            // set the default process to the always last instance of process
            defaultProcess = process;
        }

        void connectToHost(const QString &address, quint16 port);

        Process *const process;

        QtPdCom::MessageManager messageManager;

        QString appName; /**< Our application name, that is announced to the
                           server. Default: QtPdCom1.  */

        QUrl url;
        SslCaMode caMode = SslCaMode::NoTLS;
        QList<QSslCertificate> caCertificates;
        QSslKey privateKey;
        QSslCertificate privateCert;
        QSslSocket socket; /**< TCP socket to the process. */
        // make backup from inital ssl config
        const QSslConfiguration defaultSslConfig;

        bool socketValid; /**< Connection state of the socket. */
        ConnectionState connectionState; /**< The current connection state. */
        QString errorString; /**< Error reason. Set, before error() is
                               emitted. */
        quint64 rxBytes;
        quint64 txBytes;

        QQueue<VariablePromise> findVariableQueue;
        VariablePromise currentFindVariablePromise;
        QQueue<ListPromise> listVariableQueue;
        ListPromise currentListPromise;
        QQueue<PingPromise> pingQueue;
        QQueue<ClientStatisticsPromise> clientStatisticsQueue;

        static QtPdCom::Process *defaultProcess; /**< last created process
                                              is the default process */

        void setConnectionState(ConnectionState const state)
        {
            if (connectionState != state) {
                connectionState = state;
                emit process->connectionStatusChanged();
            }
        }
};

/****************************************************************************/

QtPdCom::Process *QtPdCom::Process::Impl::defaultProcess = nullptr;

/*****************************************************************************
 * Public implementation
 ****************************************************************************/

/** Constructor.
 */
Process::Process(QObject *parent):
    QObject(parent),
    PdCom::Process(),
    impl {std::unique_ptr<Impl> {new Impl {this}}}
{
    connect(&impl->socket,
            SIGNAL(connected()),
            this,
            SLOT(socketConnected()));
    connect(&impl->socket,
            SIGNAL(disconnected()),
            this,
            SLOT(socketDisconnected()));
#if QT_VERSION >= QT_VERSION_CHECK(5, 15, 0)
    connect(&impl->socket,
            &QAbstractSocket::errorOccurred,
            this,
            &Process::socketError);
#else
    connect(&impl->socket,
            SIGNAL(error(QAbstractSocket::SocketError)),
            this,
            SLOT(socketError()));
#endif
    connect(&impl->socket, SIGNAL(readyRead()), this, SLOT(socketRead()));

    setMessageManager(&impl->messageManager);
}

/****************************************************************************/

/** Destructor.
 */
Process::~Process()
{
    setMessageManager(nullptr);

    // reset default process if this instance is the default
    if (impl->defaultProcess == this) {
        impl->defaultProcess = nullptr;
    }

    disconnectFromHost();
    reset();
}

/****************************************************************************/

/** Sets the application name.
 */
void Process::setApplicationName(const QString &name)
{
    impl->appName = name;
}

QString Process::getApplicationName() const
{
    return impl->appName;
}

QVariant Process::nameQt() const
{
    try {
        return QString::fromStdString(name());
    }
    catch (PdCom::NotConnected const &) {
        return {};
    }
}

QVariant Process::versionQt() const
{
    try {
        return QString::fromStdString(version());
    }
    catch (PdCom::NotConnected const &) {
        return {};
    }
}

/****************************************************************************/

/** Starts to connect to a process.
 */
void Process::connectToHost(const QString &address, quint16 port)
{
    impl->connectToHost(address, port);
}

void Process::Impl::connectToHost(const QString &address, quint16 port)
{
    url.setHost(address);
    url.setPort(port);
    url.setScheme(caMode == SslCaMode::NoTLS ? "msr" : "msrs");
    setConnectionState(Connecting);
    if (caMode == SslCaMode::NoTLS) {
        socket.connectToHost(address, port);
        return;
    }
    auto ssl_config = defaultSslConfig;
    ssl_config.setPeerVerifyMode(QSslSocket::VerifyPeer);
    if (caMode == SslCaMode::CustomCAs) {
        ssl_config.setCaCertificates(caCertificates);
    }
    else if (caMode == SslCaMode::IgnoreCertificate) {
        ssl_config.setPeerVerifyMode(QSslSocket::VerifyNone);
    }
    if (!privateCert.isNull() && !privateKey.isNull()) {
        ssl_config.setPrivateKey(privateKey);
        ssl_config.setLocalCertificate(privateCert);
    }
    socket.setSslConfiguration(ssl_config);
    socket.connectToHostEncrypted(address, port);
}

/****************************************************************************/

/** Disconnects from a process.
 */
void Process::disconnectFromHost()
{
    switch (impl->connectionState) {
        case Connecting:
        case Connected:
            impl->socketValid = false;
            impl->setConnectionState(Disconnected);
            asyncData();
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            impl->socket.disconnectFromHost();
            impl->socket.abort();
            emit disconnected();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/**
 * \return The connection state.
 */
Process::ConnectionState Process::getConnectionState() const
{
    return impl->connectionState;
}

/****************************************************************************/

/**
 * \return \a true, if the process is connected.
 */
bool Process::isConnected() const
{
    return impl->connectionState == Connected;
}

/****************************************************************************/

/**
 * \return Error reason after the error() signal was emitted.
 */
const QString &Process::getErrorString() const
{
    return impl->errorString;
}

/****************************************************************************/

/**
 * \return Host name of the process.
 */
QString Process::getPeerName() const
{
    return impl->socket.peerName();
}

/****************************************************************************/

/** Wrapper function for Process::findVariable.
 */

QFuture<PdCom::Variable> Process::find(const QString &path)
{
    impl->currentFindVariablePromise = {};
    impl->currentFindVariablePromise.reportStarted();
    auto ans = impl->currentFindVariablePromise.future();
    if (!PdCom::Process::find(path.toStdString())) {
        // variable was not cached, findReply() will be called later
        impl->findVariableQueue.append(impl->currentFindVariablePromise);
    }
    impl->currentFindVariablePromise = {};
    return ans;
}

/****************************************************************************/

/** Wrapper function for Process::list.
 */

QFuture<QtPdCom::VariableList> Process::list(const QString &path)
{
    impl->currentListPromise = {};
    impl->currentListPromise.reportStarted();
    auto ans = impl->currentListPromise.future();
    if (!PdCom::Process::list(path.toStdString())) {
        impl->listVariableQueue.append(impl->currentListPromise);
    }
    impl->currentListPromise = {};
    return ans;
}

/****************************************************************************/

QFuture<std::vector<PdCom::ClientStatistics>>
QtPdCom::Process::getClientStatisticsQt()
{
    ClientStatisticsPromise ans;
    impl->clientStatisticsQueue.append(ans);
    ans.reportStarted();
    PdCom::Process::getClientStatistics();
    return ans.future();
}

/****************************************************************************/

/** Send a broadcast message.
 */
void Process::sendBroadcast(const QString &msg, const QString &attr)
{
    broadcast(msg.toStdString(), attr.toStdString());
}

/****************************************************************************/

quint64 Process::getRxBytes() const
{
    return impl->rxBytes;
}

/****************************************************************************/

quint64 Process::getTxBytes() const
{
    return impl->txBytes;
}

void QtPdCom::Process::setCaMode(SslCaMode mode)
{
    if (impl->caMode != mode) {
        impl->caMode = mode;
        impl->url.setScheme(mode == SslCaMode::NoTLS ? "msr" : "msrs");
        emit sslCaModeChanged();
    }
}

Process::SslCaMode Process::getCaMode() const
{
    return impl->caMode;
}


void QtPdCom::Process::setClientCertificate(
        const QSslCertificate &cert,
        const QSslKey &key)
{
    impl->privateKey = key;
    impl->privateCert = cert;
}

void QtPdCom::Process::setCustomCAs(QList<QSslCertificate> cas)
{
    impl->caCertificates = std::move(cas);
}

/****************************************************************************/

QtPdCom::Process *Process::getDefaultProcess()
{
    return QtPdCom::Process::Impl::defaultProcess;
}

/****************************************************************************/

/** Set default process "manually"
 */
void Process::setDefaultProcess(QtPdCom::Process *process)
{
    QtPdCom::Process::Impl::defaultProcess = process;
}

/****************************************************************************/

PdCom::MessageManagerBase *Process::getMessageManager() const
{
    return &impl->messageManager;
}

/****************************************************************************/

void QtPdCom::Process::setLoginManager(LoginManager *lm)
{
    Q_UNUSED(lm)

#ifdef QTPDCOM_HAS_LOGIN_MANAGER
    if (lm) {
        setAuthManager(lm->d_ptr.data());
    }
    else {
        setAuthManager(nullptr);
    }
#else
    throw PdCom::InvalidArgument("build without sasl support");
#endif
}

/****************************************************************************/

QtPdCom::LoginManager *Process::getLoginManager() const
{
#ifdef QTPDCOM_HAS_LOGIN_MANAGER
    if (const auto p = getAuthManager()) {
        auto *lm_p = static_cast<LoginManagerPrivate *>(p);
        return lm_p->q_ptr;
    }
    return nullptr;
#else
    throw PdCom::InvalidArgument("build without sasl support");
#endif
}

/*****************************************************************************
 * private methods
 ****************************************************************************/

std::string Process::applicationName() const
{
    return impl->appName.toStdString();
}

/****************************************************************************/

std::string Process::hostname() const
{
    char hostname[256];
    if (!gethostname(hostname, sizeof(hostname))) {
        return hostname;
    }
    return "";
}

/****************************************************************************/

/** Read data from the socket.
 */
int Process::read(char *buf, int count)
{
    if (impl->connectionState == Disconnected
        || impl->connectionState == ConnectedError) {
        return 0;
    }
    qint64 ret(impl->socket.read(buf, count));
    if (ret > 0) {
        impl->rxBytes += ret;
    }
    return ret;
}

/****************************************************************************/

/** Sends data via the socket.
 *
 * This is the implementation of the virtual PdCom::Process
 * method to enable the Process object to send data to the process.
 */
void Process::write(const char *buf, size_t count)
{
#if DEBUG_DATA
    qDebug() << "Writing:" << count << QByteArray(buf, count);
#endif
    while (count > 0) {
        qint64 ret(impl->socket.write(buf, count));
        if (ret <= 0) {
            qWarning("write() failed.");
            impl->socketValid = false;
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            impl->socket.disconnectFromHost();
            emit error();
            return;
        }
        count -= ret;
        buf += ret;
        impl->txBytes += ret;
    }
}

/****************************************************************************/

/** Flushed the socket.
 */
void Process::flush()
{
#if DEBUG_DATA
    qDebug() << "Flushing not implemented.";
#endif
}

/****************************************************************************/

/** The process is connected and ready.
 *
 * This virtual function from PdCom::Process has to be overloaded to let
 * subclasses know about this event.
 */
void Process::connected()
{
    impl->setConnectionState(Connected);
    emit processConnected();
}

/****************************************************************************/

/** Broadcast Reply.
 */
void Process::broadcastReply(
        const std::string &message,
        const std::string &attr,
        std::chrono::nanoseconds time_ns,
        const std::string &user)

{
    emit broadcastReceived(
            QString::fromStdString(message),
            QString::fromStdString(attr),
            time_ns.count(),
            QString::fromStdString(user));
}

/****************************************************************************/

/** Ping Reply.
 */
void Process::pingReply()
{
    if (impl->pingQueue.empty()) {
        return;
    }

    auto promise = impl->pingQueue.dequeue();
    promise.reportFinished();
}

/****************************************************************************/

QFuture<void> Process::pingQt()
{
    PingPromise promise;
    promise.reportStarted();
    impl->pingQueue.push_back(promise);
    PdCom::Process::ping();
    return promise.future();
}

/****************************************************************************/

/** Resets the PdCom process.
 */
void Process::reset()
{
    try {
        PdCom::Process::reset();
    }
    catch (std::exception &e) {
        // do nothing
    }

    impl->messageManager.reset();
    // cancel all pending futures
    while (!impl->pingQueue.empty()) {
        impl->pingQueue.dequeue().reportCanceled();
    }
    while (!impl->findVariableQueue.empty()) {
        impl->findVariableQueue.dequeue().reportCanceled();
    }
    if (impl->currentFindVariablePromise.isRunning()) {
        impl->currentFindVariablePromise.reportCanceled();
        impl->currentFindVariablePromise = {};
    }

    while (!impl->listVariableQueue.empty()) {
        impl->listVariableQueue.dequeue().reportCanceled();
    }
    if (impl->currentListPromise.isRunning()) {
        impl->currentListPromise.reportCanceled();
        impl->currentListPromise = {};
    }
    while (!impl->clientStatisticsQueue.empty()) {
        impl->clientStatisticsQueue.dequeue().reportCanceled();
    }
}

/****************************************************************************/

/** Socket connection established.
 *
 * This is called, when the pure socket connection was established and the
 * Process object can start using it.
 */
void Process::socketConnected()
{
    impl->socketValid = true;
    impl->socket.setSocketOption(QAbstractSocket::KeepAliveOption, 1);
}

/****************************************************************************/

/** Socket disconnected.
 *
 * The socket was closed and the process has to be told, that it is
 * disconnected.
 */
void Process::socketDisconnected()
{
    switch (impl->connectionState) {
        case Connecting:
            impl->socketValid = false;
            impl->setConnectionState(ConnectError);
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        case Connected:
            impl->socketValid = false;
            impl->setConnectionState(Disconnected);
            asyncData();
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit disconnected();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/** There was a socket error.
 *
 * The error could come up either while connecting the socket, or when the
 * socket was already connected.
 */
void Process::socketError()
{
    impl->errorString = impl->socket.errorString();

    switch (impl->connectionState) {
        case Connecting:
            impl->socketValid = false;
            impl->setConnectionState(ConnectError);
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        case Connected:
            impl->socketValid = false;
            impl->setConnectionState(ConnectedError);
            asyncData();
            impl->rxBytes = 0;
            impl->txBytes = 0;
            reset();
            emit error();
            break;

        default:
            break;
    }
}

/****************************************************************************/

/** The socket has new data to read.
 */
void Process::socketRead()
{
    try {
        while (impl->socket.bytesAvailable() > 0) {
            asyncData();
        }
    }
    catch (std::exception &e) {
        impl->errorString = "Exception during asyncData(): ";
        impl->errorString += e.what();
        qCritical() << impl->errorString;
        impl->socketValid = false;
        if (impl->connectionState == Connected) {
            impl->setConnectionState(ConnectedError);
        }
        else {
            impl->setConnectionState(ConnectError);
        }
        reset();
        impl->socket.disconnectFromHost();
        emit error();
    }
    catch (...) {
        impl->errorString = "Unknown exception during asyncData()";
        qCritical() << impl->errorString;
        impl->socketValid = false;
        if (impl->connectionState == Connected) {
            impl->setConnectionState(ConnectedError);
        }
        else {
            impl->setConnectionState(ConnectError);
        }
        reset();
        impl->socket.disconnectFromHost();
        emit error();
    }
}

/****************************************************************************/


void Process::findReply(const PdCom::Variable &var)
{
    if (impl->currentFindVariablePromise.isRunning()) {
        // variable is cached, result of PdCom::Process::find() will be true
        impl->currentFindVariablePromise.reportResult(var);
        impl->currentFindVariablePromise.reportFinished();
    }
    else if (!impl->findVariableQueue.empty()) {
        auto promise = impl->findVariableQueue.dequeue();
        promise.reportResult(var);
        promise.reportFinished();
    }
}

void Process::listReply(
        const std::vector<PdCom::Variable> vars,
        const std::vector<std::string> dirs)
{
    ListPromise promise;
    if (impl->currentListPromise.isRunning()) {
        // list is cached, result of PdCom::Process::list() will be true
        promise = impl->currentListPromise;
    }
    else if (impl->listVariableQueue.empty()) {
        return;
    }
    else {
        promise = impl->listVariableQueue.dequeue();
    }
    QVector<QString> qt_dirs;
    qt_dirs.reserve(dirs.size());
    for (const auto &s : dirs) {
        qt_dirs.append(QString::fromStdString(s));
    }
    QVector<PdCom::Variable> qt_vars;
    qt_vars.resize(vars.size());
    std::copy(vars.begin(), vars.end(), qt_vars.begin());
    promise.reportResult(
            VariableList {std::move(qt_dirs), std::move(qt_vars)});
    promise.reportFinished();
}

QUrl Process::getUrl() const
{
    return impl->url;
}

int Process::getPort() const
{
    return impl->url.port();
}

QString Process::getHost() const
{
    return impl->url.host();
}

void QtPdCom::Process::clientStatisticsReply(
        std::vector<PdCom::ClientStatistics> statistics)
{
    if (impl->clientStatisticsQueue.empty()) {
        return;
    }

    auto promise = impl->clientStatisticsQueue.dequeue();
    promise.reportResult(statistics);
    promise.reportFinished();
}

namespace {

using QtPdCom::details::takes_obj_as_first_parameter;

struct A
{};

struct F1
{
        int operator()(A &a, int);
};
struct F2
{
        void operator()(int);
};

struct F3
{
        void operator()();
};

struct F4
{
        void operator()(A &);
};

template <class Fn, class Arg, class Param, class = void>
struct is_well_formed: std::false_type
{};

template <class Fn, class Arg, class Param> struct is_well_formed<
        Fn,
        Arg,
        Param,
        typename std::enable_if<
                takes_obj_as_first_parameter<Fn, Arg, Param>()
                || true>::type>: std::true_type
{};

static_assert(
        takes_obj_as_first_parameter<F1, A, int>(),
        "F1 takes object ref and int");
static_assert(
        !takes_obj_as_first_parameter<F2, A, int>(),
        "F2 only takes int");


static_assert(
        !takes_obj_as_first_parameter<F3, A>(),
        "F3 takes no arguments");
static_assert(
        takes_obj_as_first_parameter<F4, A>(),
        "F4 takes obj ref as argument");

static_assert(is_well_formed<F1, A, int>::value, "");
static_assert(is_well_formed<F2, A, int>::value, "");
static_assert(
        !is_well_formed<F3, A, int>::value,
        "Mismatch beween expected arguments and actual lambda arguments is "
        "detected");

}  // namespace
