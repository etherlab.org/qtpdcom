/*****************************************************************************
 *
 * Copyright (C) 2009 - 2022  Bjarne von Horn <vh@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ClientStatisticsModel.h"

#include "Process.h"
#include <QDateTime>

using QtPdCom::ClientStatisticsModel;
using QtPdCom::ClientStatisticsModelPrivate;

namespace QtPdCom {
struct ClientStatisticsModelPrivate
{
    ClientStatisticsModelPrivate(ClientStatisticsModel *q_ptr): q_ptr(q_ptr)
    {}

    Q_DECLARE_PUBLIC(ClientStatisticsModel)
    ClientStatisticsModel *q_ptr;
    Process *process = nullptr;
    std::vector<PdCom::ClientStatistics> statistics;
};

}  // namespace QtPdCom

ClientStatisticsModel::ClientStatisticsModel(QObject *parent):
    QAbstractTableModel(parent), d_ptr(new ClientStatisticsModelPrivate(this))
{}

ClientStatisticsModel::~ClientStatisticsModel() = default;

int ClientStatisticsModel::rowCount(const QModelIndex &) const
{
    const Q_D(ClientStatisticsModel);
    return d->statistics.size();
}

int ClientStatisticsModel::columnCount(const QModelIndex &) const
{
    return 5;
}

QVariant ClientStatisticsModel::data(const QModelIndex &index, int role) const
{
    const Q_D(ClientStatisticsModel);

    if (index.row() < 0 or index.row() >= int(d->statistics.size()))
        return QVariant();
    if (index.column() < 0 or index.column() >= 5)
        return QVariant();
    if (role != Qt::DisplayRole)
        return QVariant();

    auto column = index.column();
    if (NameRole <= role && role <= ConnectedTimeRole) {
        column = role - NameRole;
    }

    switch (column) {
        case 0:
            return QString::fromStdString(d->statistics[index.row()].name_);
        case 1:
            return QString::fromStdString(
                    d->statistics[index.row()].application_name_);
        case 2:
            return static_cast<qulonglong>(
                    d->statistics[index.row()].received_bytes_);
        case 3:
            return static_cast<qulonglong>(
                    d->statistics[index.row()].sent_bytes_);
        case 4: {
            QDateTime dt;
            dt.setSecsSinceEpoch(std::chrono::duration_cast<std::chrono::seconds>(
                                 d->statistics[index.row()].connected_time_)
                                 .count());
            return dt.toString("yyyy-MM-dd hh:mm:ss");
        }
        default:
            break;
    }
    return QVariant();
}

QVariant ClientStatisticsModel::headerData(
        int section,
        Qt::Orientation o,
        int role) const
{
    if (role == Qt::DisplayRole && o == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("Name");
            case 1:
                return tr("Application");
            case 2:
                return tr("Received bytes");
            case 3:
                return tr("Sent bytes");
            case 4:
                return tr("Connected time");
            default:
                break;
        }
    }
    return QVariant();
}

void ClientStatisticsModel::poll()
{
    Q_D(ClientStatisticsModel);

    if (!d->process)
        return;

    d->process->getClientStatistics(
            this,
            [this, d](std::vector<PdCom::ClientStatistics> statistics) {
                this->beginResetModel();
                d->statistics = std::move(statistics);
                this->endResetModel();
            });
}

void QtPdCom::ClientStatisticsModel::clear()
{
    Q_D(ClientStatisticsModel);

    beginResetModel();
    d->statistics.clear();
    endResetModel();
}

void ClientStatisticsModel::setProcess(QtPdCom::Process *process)
{
    Q_D(ClientStatisticsModel);

    d->process = process;
    if (process && process->isConnected())
        poll();
}

QtPdCom::Process *ClientStatisticsModel::getProcess() const
{
    const Q_D(ClientStatisticsModel);
    return d->process;
}

QHash<int, QByteArray> ClientStatisticsModel::roleNames() const
{
    auto ans = QAbstractTableModel::roleNames();

    ans[NameRole] = "name";
    ans[ApplicationNameRole] = "application_name";
    ans[RxByteRole] = "received_bytes";
    ans[TxByteRole] = "sent_bytes";
    ans[ConnectedTimeRole] = "connected_time";
    return ans;
}
