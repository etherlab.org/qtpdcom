/*****************************************************************************
 *
 * Copyright (C) 2009-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MainWindow.h"

#include <QtPdCom1/Translator.h>

#include <QApplication>
#include <QMainWindow>
#include <QTranslator>

/****************************************************************************/

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QTranslator translator;
    app.installTranslator(&translator);

    if (!QtPdCom::loadTranslation(translator))  {
        qWarning() << "Failed to load QtPdCom translation for locale"
            << QLocale().nativeLanguageName();
    }

    MainWindow mainWin(translator);
    mainWin.show();
    return app.exec();
}

/****************************************************************************/
