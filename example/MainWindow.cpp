/*****************************************************************************
 *
 * Copyright (C) 2012-2022  Florian Pose <fp@igh.de>
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom Library. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MainWindow.h"

#include "QtPdCom1/Translator.h"

#include <pdcom5/Subscription.h>

#include <QMessageBox>
#include <QDebug>

#include <chrono>
using namespace std::chrono_literals;

#define PERIOD 10ms

/****************************************************************************/

MainWindow::MainWindow(QTranslator& translator, QWidget *parent):
    QMainWindow(parent),
    translator(translator)
{
    setupUi(this);

    connect(&p, SIGNAL(processConnected()), this, SLOT(processConnected()));
    connect(&p, SIGNAL(disconnected()), this, SLOT(processDisconnected()));
    connect(&p, SIGNAL(error()), this, SLOT(processError()));

    actionDisconnect->setEnabled(0);
    actionConnect->trigger();

    messageModel.setIcon(
            QtPdCom::Message::Critical, QIcon(":/images/dialog-error.svg"));
    messageModel.setIcon(
            QtPdCom::Message::Error, QIcon(":/images/dialog-error.svg"));
    messageModel.setIcon(
            QtPdCom::Message::Warning, QIcon(":/images/dialog-warning.svg"));
    messageModel.setIcon(
            QtPdCom::Message::Information, QIcon(":/images/dialog-notice.svg"));
    tableViewMessages->setModel(&messageModel);

    QHeaderView *hdr = tableViewMessages->horizontalHeader();
    hdr->setMinimumSectionSize(240);
    hdr->setSectionResizeMode(0, QHeaderView::Stretch);
    hdr->setSectionResizeMode(1, QHeaderView::Fixed);
    tableViewMessages->verticalHeader()->hide();
}

/****************************************************************************/

MainWindow::~MainWindow()
{
    p.disconnectFromHost();
}

/****************************************************************************/

void MainWindow::processConnected()
{
    QString path;
    PdCom::Selector all;

    actionDisconnect->setEnabled(true);

    messageModel.connect(&p);
}

/****************************************************************************/

void MainWindow::processDisconnected()
{
    actionConnect->setEnabled(true);
    actionDisconnect->setEnabled(false);
}

/****************************************************************************/

void MainWindow::processError()
{
    actionConnect->setEnabled(true);

    QMessageBox::critical(this, tr("Connection error"),
            tr("Failed to connect to data source."));
}

/****************************************************************************/

void MainWindow::on_actionConnect_triggered()
{
    actionConnect->setEnabled(false);
    p.connectToHost("localhost", 2345);
}

/****************************************************************************/

void MainWindow::on_actionDisconnect_triggered()
{
    actionDisconnect->setEnabled(false);
    p.disconnectFromHost();
}

/****************************************************************************/

void MainWindow::on_actionGerman_triggered()
{
    QCoreApplication::removeTranslator(&translator);
    QtPdCom::loadTranslation(translator, QLocale::German);
    QCoreApplication::installTranslator(&translator);
}

/****************************************************************************/

void MainWindow::on_actionEnglish_triggered()
{
    QCoreApplication::removeTranslator(&translator);
    QtPdCom::loadTranslation(translator, QLocale::English);
    QCoreApplication::installTranslator(&translator);
}

/****************************************************************************/

void MainWindow::on_actionDutch_triggered()
{
    QCoreApplication::removeTranslator(&translator);
    QtPdCom::loadTranslation(translator, QLocale::Dutch);
    QCoreApplication::installTranslator(&translator);
}

/****************************************************************************/

void MainWindow::changeEvent(QEvent *event)
{
    if (0 != event) {
        switch (event->type()) {
            // this event is send if a translator is loaded
            case QEvent::LanguageChange:
                retranslateUi(this);
                break;
            default:
                break;
        }
    }

    QMainWindow::changeEvent(event);
}

/****************************************************************************/
